// Task1 (if, else)
//implement the function described above, here
// function wasItFunny(bool){
//     if(bool){
//         return "HAHAHA"
//     }
//         return "meh"
// }
// wasItFunny(true)
// wasItFunny(false)


// Task2 (string)
// Write a function isWordLong which takes in a string argument (input value),
// and returns true if the string is longer than 15 characters,
// and false if the string is less than or equal to 15 characters.
// You can assume that the input string will only contain letters.
// Calling your function should result in:
// isWordLong('absentmindedness'); //true
// isWordLong('aerodynamics'); //false
// let str = 'ashsfjhagjhadaaff'
// function isWordLong(str){
//   if(str.length>15){
//     return true
//   }
//     return false
// }
// isWordLong(str)


// Task3 (Object)
// Write a function addGenre which takes in an object and astring.
// This function will add a property "genre", and assign to it the input string
// Calling your function should result in something like:
// function addGenre(object, string) {
//     object['genre'] = string
//     return object
// }
// var favoriteSong = {
//     title: "Countdown",
//     artist: "Beyonce",
//     durationInSeconds: 212
// };
// addGenre(favoriteSong, "Contemporary R&B");
// console.log(favoriteSong); //{title: "Countdown", artist: "Beyonce", durationInSeconds: 212, genre: "Contemporary R&B"};


// Task4(objects)
// Реализовать функцию полного клонирования объекта. Задача должна быть реализована на языке javascript,
//     без использования фреймворков и сторонник библиотек (типа Jquery).
// #### Технические требования:
//     - Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке,
//     внутренняя вложенность свойств объекта может быть достаточно большой).
// - Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
// - В коде нельзя использовать встроенные механизмы клонирования, такие как функция `Object.assign()`
// или спред-оператор.
// const objectForClone = {
//     a: {
//         1: 'a1',
//         2: 'a2',
//         3: 'a3',
//     },
//     b: {
//         1: 'b1',
//         2: 'b2',
//         3: 'b3',
//     },
//     c: {
//         1: 'c1',
//         2: 'c2',
//         3: 'c3',
//     },
// };
// function cloneObject(object) {
//     const copy = { ...object }
//     const { b: newObject, ...rest } = object
//     return newObject
// }
// cloneObject(objectForClone)
// // another solution 
// function cloneObject(object) {
//      const copy ={}
//     for(let key in object){
//        console.log(typeof(object[key]))
//        if(typeof(object[key])==="object"){
//          copy[key]=cloneObject(object[key])
//        }else{
//          copy[key]=object[key]
//        }
//      }
//     return copy
// }
// cloneObject(objectForClone)


// Task5(arrays, spread, rest, destructirisation)
// const numbers = [2, 5, 43, 56, 78]
// const number = [11, 13, 35, 78, 99]
// const [numberThree, numberFive, numberFortyThree, ...rest] = numbers
// console.log(numberThree)
// console.log(rest)
// const newNumbers = [...numbers, ...number]
// console.log(newNumbers)


//task6(strings, arrays, numbers)
// Given a string of even and odd numbers, find which is the sole even number or the sole odd number.
// The return value should be 1-indexed, not 0-indexed.
// Examples :
// detectOutlierValue("2 4 7 8 10"); // => 3 - Third number is odd, while the rest of the numbers are even
// // detectOutlierValue("1 10 1 1");  //=> 2 - Second number is even, while the rest of the numbers are odd
// const string = "2 4 7 8 10"  // => 3 - Third number is odd, while the rest of the numbers are even
// const string = "1 10 1 1"
// function findNumber(string) {
//     var even = 0
//     var odd = 0
//     var evenCounter = 0
//     var oddCounter = 0
//     var splitted = string.split(' ')
//     var index = 0
//     for (var i = 0; i < splitted.length; i++) {
//         if (splitted[i] % 2 === 0) {
//             even = splitted[i]
//             evenCounter++
//         }
//         if (splitted[i] % 2 === 1) {
//             odd = splitted[i]
//             oddCounter++
//         }
//     }
//     if (evenCounter === 1) {
//         for (var i = 0; i < splitted.length; i++) {
//             if (splitted[i] === even) {
//                 index = i+1
//             }
//         }
//     }
//     if (oddCounter === 1) {
//         for (var i = 0; i < splitted.length; i++) {
//             if (splitted[i] === odd) {
//                 index = i+1
//             }
//         }
//     }
//     return index
// }
// findNumber(string)

// Task6_1(one way solution/odd version)
// const string = "2 4 5 8 6"
// function findNumber(string){
//   const array = string.split(' ')
// console.log(array)
// let count = null
// array.filter((item, index)=>{
//   if(item%2===1){
//   count= index+1
//   }
//    })
//    return count
// }
// findNumber(string)
//forEach method
// const string = "2 4 5 8 6"
// function findNumber(string){
//   const array = string.split(' ')
// console.log(array)
// let count = null
// array.forEach((item, index)=>{
//   if(item%2===1){
//   count= index+1
//   }
//    })
//    return count
// }
// findNumber(string)
// filter method
// const string = "2 4 5 8 6"
// function findNumber(string){
//   const array = string.split(' ')
// console.log(array)
// let count = null
// array.filter((item, index)=>{
//   if(item%2===1){
//   count= index+1
//   }
//    })
//    return count
// }
// findNumber(string)



// Task7
// Your current client is a casino that needs you to fix their Black-Jack card counting program. 
//The function should take an array of objects, each object is a card 
//that was dealt and its value is the number on the card.
// The way card counting works is that each card has a weighted value. 
//Cards 2 through 6 are worth one point; 7, 8 and 9 are worth zero; and 10s, 
//face cards and Aces are worth negative one. 
//This type of card-counting system is called the HiLo count. 
//When counting down a standard deck of 52 cards, you'll notice the count will go up and down, 
//and always end at zero if you counted correctly.
// The cardCounter function is supposed to take an array of card objects 
//and return the count (weighted value) of the cards.
// It doesn't seem to be producing accurate results, see if you can fix it before the card-sharks beat the house!
// our input
// const cards = [
//   {2:1},
//   {3:1},
//   {j:-1},
//   {a:-1},
//   {10:-1},
//   {7:0},
//   {8:0},
//   {q:-1},
//   {k:-1},
//   {4:1},
//   {5:1},
//   {6:1},
//   {9:0},
//     {2:1},
//   {3:1},
//   {j:-1},
//   {a:-1},
//   {10:-1},
//   {7:0},
//   {8:0},
//   {q:-1},
//   {k:-1},
//   {4:1},
//   {5:1},
//   {6:1},
//   {9:0}, 
//    {2:1},
//   {3:1},
//   {j:-1},
//   {a:-1},
//   {10:-1},
//   {7:0},
//   {8:0},
//   {q:-1},
//   {k:-1},
//   {4:1},
//   {5:1},
//   {6:1},
//   {9:0}, 
//    {2:1},
//   {3:1},
//   {j:-1},
//   {a:-1},
//   {10:-1},
//   {7:0},
//   {8:0},
//   {q:-1},
//   {k:-1},
//   {4:1},
//   {5:1},
//   {6:1},
//   {9:0},  
// ]
// function cardCounter(array){
//   let count = 0
//   for(let i=0; i<array.length; i++){
//  for(let key in array[i]){
//   count+=array[i][key]
//  }
//   }
//  array.forEach(item=>{
//    for(let key in item){
//      count+=item[key]
//    }
//  })
//   return count
// }
// const expected = cardCounter(cards)
// function assertEqual(actual, expected, testName){
//   if(actual===expected){
//     console.log('passed')
//   }else{
//     console.log(`FAIL ${testName} Expected "${expected}", but got ${actual}` )
//   }
// }
// assertEqual(0, expected, 'casinoClient' )
// const cards = [
//   {2:1},
//   {3:1},
//   {j:-1},
//   {a:-1},
//   {10:-1},
//   {7:0},
//   {8:0},
//   {q:-1},
//   {k:-1},
//   {4:1},
//   {5:1},
//   {6:1},
//   {9:0},
// ]
// original input
// const dealtCards = [ { 'card 0': 2 }, { 'card 1': 6 }, { 'card 2': 8 }, { 'card 3': 'face or ace' } ];
// cardCounter(dealtCards)
// function cardCounter(array) {
//     let count = 0;
//     for (let i = 0; i < array.length; i++) {
// for(let key in array[i]){

// if(array[i][key]>=2 && array[i][key]<=6){
//   return count+=1
// }else{
// return count-=1
// }
// }
// if (card.card + i >= 2 && card.card + i <= 6) {
//     count++;
// } else if (card.card + i >= 10 || card.card + i === 'face or ace') {
//     count--;
// }
//     }
//     console.log(count)
//     return count
// }
// function assertEquals(actual, expected, testDescription) {
//     if (actual !== expected) {
//         console.log('Test failed:', testDescription);
//         return;
//     }
//     console.log('test passed');
//     return;
// }
// var actual = cardCounter(dealtCards);
// var expected = 1;
// assertEquals(actual, expected, 'function should return correct HiLo count.');
// var dealtCards2 = [ { 'card 0': 'face or ace' }, { 'card 1': 9 }, { 'card 2': 8 }, { 'card 3': 'face or ace' } ];
// var actual2 = cardCounter(dealtCards2);
// var expected2 = -2;
// assertEquals(actual2, expected2, 'function should return correct HiLo count.');


// Task8 (arrays)
// Создайте массив styles с элементами «Джаз» и «Блюз».
// Добавьте «Рок-н-ролл» в конец.
// Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
// Удалите первый элемент массива и покажите его.
// Вставьте «Рэп» и «Регги» в начало массива.
// const array = ["jazz", "blues"]
// array.push("RockNroll")
// console.log(array)
// array[Math.floor((array.length-1)/2)]="Classic"
// console.log(array)
// console.log(array.shift())
// console.log(array)
// array.unshift("Rap", "Reggi")
// console.log(array)


// Task9(arrays)
// Given a list of non-negative integers and a target sum, find a pair of numbers that sums to the target sum.
// Example:
// var pair = findPairForSum([3, 34, 4, 12, 5, 2], 9);
// console.log(pair); // --> [4, 5]
// function findPairForSum(array, target){
//   let pair = []
//   for(let i=0; i<array.length;i++){
//     for(let j=0; j<array.length; j++){
//       if(array[i]+array[j]===target){
//         pair.push(array[i])
//       }
//     }
//   }
//   return pair
// }
// function assertEqual(actual,expected, testName){
//   actual=JSON.stringify(actual)
//   expected = JSON.stringify(expected)
//   console.log(actual)
//   if(actual===expected){
//     console.log("passed")
//   }else{
//     console.log(`FAIL [${testName}] Expected [${expected}], but got [${actual}]`)
//   }
// }
// var actual = [ 4, 5 ]
// var expected = [ 4, 5 ]
// var testName= "findPair"
// assertEqual(actual,expected, testName)


// Task10 (algorythms)
// Технические требования:
//     - Функция на вход принимает три параметра:
//     - массив из чисел, который представляет скорость работы разных членов команды. Количество элементов
// в массиве означает количество человек в команде. Каждый элемент означает сколько стори поинтов (условная
// оценка сложности выполнения задачи) может выполнить данный разработчик за 1 день.
// - массив из чисел, который представляет собой беклог (список всех задач, которые необходимо выполнить).
// Количество элементов в массиве означает количество задач в беклоге. Каждое число в массиве означает
// количество стори поинтов, необходимых для выполнения данной задачи.
// - дата дедлайна (объект типа Date).
// После выполнения, функция должна посчитать, успеет ли команда разработчиков выполнить все задачи из
// беклога до наступления дедлайна (работа ведется начиная с сегодняшнего дня). Если да, вывести на экран
// сообщение: `Все задачи будут успешно выполнены за ? дней до наступления дедлайна!`. Подставить нужное число
// дней в текст. Если нет, вывести сообщение `Команде разработчиков придется потратить дополнительно ? часов
// после дедлайна, чтобы выполнить все задачи в беклоге`
// - Работа идет по 8 часов в день по будним дням
// const storyPoint = [5, 6, 4, 7, 3];
// const backLog = [159, 81, 264, 153]
// const date = new Date('february.12.2022')
// const today = new Date();
// function countDays() {
//     let day = 0
//     let startDay = today.getDay()
//     switch (startDay) {
//         case 0:
//             day = -1;
//             break;
//         case 1:
//             day = 0;
//             break;
//         case 2:
//             day = -6;
//             break;
//         case 3:
//             day = -5;
//             break;
//         case 4:
//             day = -4;
//             break;
//         case 5:
//             day = -3;
//             break;
//         case 6:
//             day = -2;
//             break;
//     }
//     return day
// }
// function deadLine(storyPoint, backLog, date) {
//     let sumPointDay = reduceNumbers(storyPoint)
//     let sumOfBackLog = reduceNumbers(backLog)
//     const daysNeeded = Math.ceil(sumOfBackLog / sumPointDay)
//     console.log(daysNeeded)
//     const daysBefore = Math.round((date - today) / 1000 / 60 / 60 / 24)
//     console.log(daysBefore)
//     let startWeek = countDays()
//     let dayOfEndWeek = date.getDay()
//     console.log(dayOfEndWeek)
//     console.log(startWeek)
//     const daysNoWeekends = Math.ceil((daysBefore + startWeek - dayOfEndWeek) / 7 * 5)
//     if (daysNeeded < daysNoWeekends) {
//         const daysLeftToDeadLine = daysNoWeekends - daysNeeded
//         console.log(`Bсе задачи будут успешно выполнены за ${daysLeftToDeadLine} дней до наступления дедлайна!`)
//     } else {
//         const hoursAfterDeadLine = Math.ceil((daysNeeded - daysNoWeekends) * 8)
//         console.log(`Команде разработчиков придется потратить дополнительно ${hoursAfterDeadLine} часов после дедлайна, чтобы выполнить все задачи в беклоге`)
//     }
// }
// deadLine(storyPoint, backLog, date)
// function reduceNumbers(array) {
//     return array.reduce((acc, curent) => acc + curent)
// }


// Task11(alhorythms)
// Binary search is a technique for very rapidly searching a sorted collection by cutting the search space in half at every pass.
// Given a sorted array, such as this:
// [1, 3, 16, 22, 31, 33, 34]
// You can search for the value 31, as follows:
// 1) Pick the midpoint: 22.
// 2) The value is higher than 22, so now consider only the right half of the previous array:
// [...31, 33, 34]
// 3) Pick the midpoint: 33
// 4) The value is lower than 33, so now consider only the left half of the previous array:
// [...31...]
// 5) Pick the midpoint: 31
// 6) You've hit the value.
// 7) Return the index of the value.
// Notes:
// * If you don't find the value, you can return null.
// * If at any point you calculate the index of the midpoint and get a fractional number, just round it down ("floor" it).
// const array = [1, 3, 16, 22, 31, 33, 34]
// const number = 3
// function binerySearch(array, number){
//   let first = 0
//   let last =array.length-1
//   let position = null
//   let found = false
//   let middle 
//   while(found===false && first>= last){
//     middle = Math.floor((first+last)/2)
//     if(array[middle]===number){
//       found = true
//       position = middle
//     }else if(array[middle]>number){
//       last = middle-1
//     }else{
//       last=middle+1
//     }
//     return position
// }
// Another solution
// let midPoint =Math.floor((array.length)/2)
// //console.log(array[midPoint])
// if(array[midPoint]<number){
//   const newArray = array.slice(midPoint)
//  binerySearch(newArray, number)
// }else if(array[midPoint]>number){
//   const newArray = array.slice(0,midPoint)
//   //console.log(midPoint)
//  binerySearch(newArray, number)
// }else if(array[midPoint]===number) {
//   console.log(midPoint)
//   return "find"
// }else{
//   return null
// }
// }
// binerySearch(array, number)


//Task11(object, array)
// Write a function called transformEmployeeData that transforms some employee data from one format to another.
// The argument will look like this:
// var input = [
//     [
//         ['firstName', 'Joe'], ['lastName', 'Blow'], ['age', 42], ['role', 'clerk']
//     ],
//     [
//         ['firstName', 'Mary'], ['lastName', 'Jenkins'], ['age', 36], ['role', 'manager']
//     ]
// ];
// Given that input, the return value should look like this:
// var result = [
//     { firstName: 'Joe', lastName: 'Blow', age: 42, role: 'clerk' },
//     { firstName: 'Mary', lastName: 'Jenkins', age: 36, role: 'manager' }
// ]
// Note that the input may have a different number of rows(more employees) or different keys than the given sample.
// var input = [
//     [
//         ['firstName', 'Joe'], ['lastName', 'Blow'], ['age', 42], ['role', 'clerk']
//     ],
//     [
//         ['firstName', 'Mary'], ['lastName', 'Jenkins'], ['age', 36], ['role', 'manager']
//     ]
// ];
// function transformEmployeeData(employeeData) {
//     // your code here
//     var result = employeeData.map(item => {
//         var object = {}
//         var result1 = item.forEach(person => {
//             object[person[0]] = person[1]
//         })
//         return object
//     })
//     return result
// }
// transformEmployeeData(input)


// Task12(array, object)
// Convert an Object to a Complex Array
// Complete a function called convertObjectToArray which converts an object literal into an array of arrays, like this:
// Argument:
// var input = {
//     name: 'Holly',
//     age: 35,
//     role: 'producer'
// }
// Return value:var output = [['name', 'Holly'], ['age', 35], ['role', 'producer']]
// Note that the input may have a different number of properties than the given sample.
// function convertObjectToArray(obj) {
//     // your code here
//     var result = []
//     for (key in obj) {
//         console.log(obj[key])
//         result.push([key, obj[key]])
//     }
//     return result
// }
// convertObjectToArray(input)


// Task13 (array, reduce)
// function countTrue(arr) {
// 	// var result = 0
//   // for (var i=0; i<arr.length; i++){
//   //   if(arr[i]){
//   //     result++
//   //   }
//   // }
//   // return result
//   var result = arr.reduce((acc, current)=>{
// return acc+current
//   },0)
//   return result
// }
// Another solution
// function countTrue(arr) {
// 	var result = 0
//   for (var i=0; i<arr.length; i++){
//     if(arr[i]){
//       result++
//     }
//   }
//   return result
// }
// countTrue([true, false, false, true, false]) // 2
// countTrue([false, false, false, false]) // 0
// countTrue([]) //0


// Task14 (array, sort) !!!!!!!!!!!!!!!
// function lonelyInteger(arr) {
// 	var sorted = arr.sort((a,b)=>{
//     return a-b
//   })
//   console.log(sorted)
// }
// lonelyInteger([1, -1, 2, -2, 3]) // 3
// // 3 has no matching negative appearance.
// lonelyInteger([-3, 1, 2, 3, -1, -4, -2]) // -4
// // -4 has no matching positive appearance.
// lonelyInteger([-9, -105, -9, -9, -9, -9, 105]) // -9


// Task 15 (array, concat)
// function concat(...args) {
//   var result =[]
//   for (var i=0; i<args.length; i++){
//     result= result.concat(args[i])
//     // result = [...args[i]]
//   }
//   return result
// }
// concat([1, 2, 3], [4, 5], [6, 7]) // [1, 2, 3, 4, 5, 6, 7]
// concat([1], [2], [3], [4], [5], [6], [7]) // [1, 2, 3, 4, 5, 6, 7]
// concat([1, 2], [3, 4]) // [1, 2, 3, 4]
// concat([4, 4, 4, 4, 4]) // [4, 4, 4, 4, 4]


// Task 16 (array, Math)
// function changeEnough(change, amountDue) {
// var result = change[0]*0.25+change[1]*0.10+change[2]*0.05+change[3]*0.01;
// if(result>=amountDue){
//   return true
// }
// return false
// }
// changeEnough([2, 100, 0, 0], 14.11) // false
// changeEnough([0, 0, 20, 5], 0.75) // true
// changeEnough([30, 40, 20, 5], 12.55) // true
// changeEnough([10, 0, 0, 50], 3.85) //false
// changeEnough([1, 0, 5, 219], 19.99) //false


// Task 17 (array, Math)
// function arrayOfMultiples (num, length) {
// 	var result =[]
//   for(i=1; i<=length; i++){
// result.push(num*i)
//   }
//   return result
// }
// arrayOfMultiples(7, 5) // [7, 14, 21, 28, 35]
// arrayOfMultiples(12, 10) // [12, 24, 36, 48, 60, 72, 84, 96, 108, 120]
// arrayOfMultiples(17, 6) // [17, 34, 51, 68, 85, 102]


// Task 18 (array, filter)
// function compact(arr) {
// var result=[]
// for(var i =0; i<arr.length; i++){
//   if(arr[i]){
//     result.push(arr[i])
//   }
// }
// return result
//}
// compact([0, 1, false, 2, "", 3]);   // => [1, 2, 3]
// Another solution
// function compact(arr) {
//   return arr.filter(item=>!!item===true)
// }
// compact([0, 1, false, 2, "", 3]);   // => [1, 2, 3]


// Task 19 (array, sort)
// function secondLargest(arr) {
// 	var sorted= arr.sort((a,b)=>a-b)
//   return sorted[sorted.length-2]
// }
// secondLargest([10, 40, 30, 20, 50]) // 40
// secondLargest([25, 143, 89, 13, 105]) //105
// secondLargest([54, 23, 11, 17, 10]) // 23


// Task 20 (array, string)
// function getStringLength(string) {
// var splitted = string.split('')
// console.log(splitted)
// var result =0
// splitted.forEach((item, i, array)=>{
//   result=i+1
//   console.log(array)
// })
// let newString = string
// console.log(newString)
// let result=0
//Another solution
// while (newString!==''){
//   newString=newString.slice(0,-1)
//   result++
// }
// return result
// }
// var output = getStringLength('hello');
// console.log(output); // --> 5


//Task 21 (number, for)
// function sumDigits(num) {
//   var string = num.toString()
//   var result=0
//     if(string[0]==='-'){
//       result=+(string[0]+string[1])
//   for(var i=2; i<string.length; i++){
//    result+= +(string[i])
//   }
//     }else{
//       for(var i=0; i<string.length;i++){
//         result += +(string[i])
//       }
//     }
//   return result
// }
// var output = sumDigits(-316);
// console.log(output); // --> 4


//Task 22 (array, obj)
// function getSumOfAllElementsAtProperty(obj, key) {
//   console.log(obj[key])
//   if(obj[key].length===0){
//     return 0
//   }
//   if(Array.isArray(obj[key])!==true){
//     return 0
//   }
//   if(obj[key]===undefined){
//       return 0
//   }
//   var result = 0
//   for (i=0; i<obj[key].length; i++){
//    result+=obj[key][i]
//   }
//   return result
// }
// var obj = {
//   key: [4, 1, 8]
// };
// var output = getSumOfAllElementsAtProperty(obj, 'key');
// console.log(output); // --> 13


//Task 23 (array)
// If there are ties, it should return the first element to appear in the given array.
// Expect the given array to have values other than strings.
// If the given array is empty, it should return an empty string.
// If the given array contains no strings, it should return an empty string.

// function findShortestWordAmongMixedElements(arr) {
//   // your code here
//   if(arr.length===0){
//     return ""
//   }
//   var shortest=arr[0]
// for(var i=1; i<arr.length; i++){
//   if(typeof arr[i]==='string'){
//     if(arr[i].length<shortest.length){
//       shortest = arr[i]
//     }
//   }
//   console.log(typeof arr[i]) 
// }
// return shortest
// }
// findShortestWordAmongMixedElements([4, 'two', 2, 'three']);


// Task 24 (array, filter)
// If the given array is empty, it should return 0.
// If the array contains no numbers, it should return 0.
// function findSmallestNumberAmongMixedElements(arr) {
//   if(arr.length===0){
//     return 0
//   }
//   var filtered = arr.filter(item=>{
//     if(typeof item ==='number'){
//       return item
//     }
//   })
// if(filtered.length===0){
//     return 0
//   }
// var smalest = filtered[0]
// for(var i =1;i<filtered.length; i++){
//   if(filtered[i]<smalest){
//     smalest=filtered[i]
//   }
// }
// return smalest
// }
// findSmallestNumberAmongMixedElements([4, 'lincoln', 9, 'octopus']);


// Task 25 (array, string)
// If the array is empty, it should return an empty string ("").
// If the array contains no strings; it should return an empty string.
// function getLongestWordOfMixedElements(arr) {
//   // your code here
//   if(arr.length===0){
//     return ""
//   }
//   var filtered= arr.filter(item=>{
//     if(typeof item === 'string'){
//       return item
//     }
//   })
//   console.log(filtered)
//   if(filtered.length===0){
//     return ""
//   }
//   var longest = filtered[0]
//   for(var i=1; i<filtered.length;i++){
//     if(filtered[i].length>longest.length){
//       longest=filtered[i]
//     }
//   }
//   return longest
// }
// var output = getLongestWordOfMixedElements([3, 'word', 5, 'up', 3, 1]);
// console.log(output); // --> 'word'


// Task 26 (array, string)
// function computeSummationToN(n) {
//   var result=0
//   for(i=0; i<=n;i++){
// result+=i
//   }
//   return result
// }
// var output = computeSummationToN(6);
// console.log(output); // --> 21
// (100 - 90) --> 'A'
// (89 - 80) --> 'B'
// (79 - 70) --> 'C'
// (69 - 60) --> 'D'
// (59 - 0) --> 'F'
// If the given score is greater than 100 or less than 0, it should return 'INVALID SCORE'.
// function convertScoreToGrade(score) {
//   if(score>100 || score<0){
//     return 'INVALID SCORE'
//   }
//   if(score<60){
//     return "F"
//   }else if(score>=60 && score<70){
//     return "D"
//   }else if(score>=70 && score <80){
//     return "C"
//   }else if(score>=80 && score<90 ){
//     return "B"
//   }else{
//     return "A"
//   }
// }
// var output = convertScoreToGrade(75);
// // console.log(output); // --> 'A'


// Task 27 (number, string)
// function repeatString(string, num) {
//   // your code here
//   var result =""
//   for(i=0; i<num; i++){
//     result+=string
//   }
//   return result
// }
// var output = repeatString('code', 3);
// console.log(output); // --> 'codecodecode'


// Task 28 (while, number)
// function isOddWithoutModulo(num) {
//  while(num>=2){
//    num-=2
//  }
//return (num)? true:false
//  if(num){
//    return true
//  }
//  return false
// }
// var output = isOddWithoutModulo(-10);
// console.log(output); // --> true


// Task 29 (while)
// function isEvenWithoutModulo(num) {
//  while(num>=1){
//    num-=2
//  }
//  if(num){
//    return true
//  }
// return false
// }
// var output = isEvenWithoutModulo(98);
// console.log(output); // --> true


// Task 30 (numbet, Math)
// function multiplyBetween(num1, num2) {
//   var result =1
//   for(i=num1;i<num2;i++){
//     result*=num1
//   }
//   return result
// }
// var output = multiplyBetween(2, 5);
// console.log(output); // --> 24
// Another solution
// function multiplyBetween(num1, num2) {
//   var result =1
//   while(num1<num2){
//     result*=num1
//      num1++
//   }
//  return result
// }
// var output = multiplyBetween(2, 5);
// console.log(output); // --> 24


//Task 31 (array, string)
// function detectOutlierValue(string) {
// var array = string.split(' ')
// console.log(array)
// for(var i=0; i<array.length; i++){
//   if(array[i]%2===1){
//     return [i+1].join('')
//   }
// }
// }
// detectOutlierValue("2 4 7 8 10"); // => 3 - Third number is odd, while the rest of the numbers are even
// // detectOutlierValue("1 10 1 1");  //=> 2 - Second number is even, while the rest of the numbers are odd


// Task 32 (array)
// function findPairForSum(array, targetSum) {
//   var result = []
//   for(var i=0; i<array.length; i++){
//     for(var j=0; j<array.length; j++){
//       if(array[i]+array[j]===targetSum)
//       result.push(array[i])
//     }
//   }
//   return result
// }
// findPairForSum([3, 34, 4, 12, 5, 2], 9);


// Task 33 (objects)
// var customerData = {
//   'Joe': {
//     visits: 1
//   },
//   'Carol': {
//     visits: 2
//   },
//   'Howard': {
//     visits: 3,
//   },
//   'Carrie': {
//     visits: 4
//   }
// };
// function greetCustomer(firstName) {
//   // your code here
//   //console.log(customerData[firstName].visits)
//   console.log(customerData)
//   if(customerData[firstName]===undefined){
//   customerData[firstName]= {visits:1}
//   return `'Welcome! Is this your first time?'`
//   }else if(customerData[firstName].visits===1){
//  customerData[firstName].visits++
//  return `Welcome back, ${firstName}! We're glad you liked us the first time!`
//   }else{
//     customerData[firstName].visits++
//    return `Welcome back, ${firstName}! So glad to see you again!`
//   }
// }
// greetCustomer('Carol')
// greetCustomer('Joe')
// greetCustomer('Dmytro')
// greetCustomer('Howard')


// Task 34 (array, sort)
// function sortIt(arr) {
// 	var sorted=arr.sort((a,b)=>{
// return a-b
//   })
//   return sorted
// }
// sortIt([4, 1, 3]) ➞ [1, 3, 4]
// sortIt([[4], [1], [3]]) ➞ [[1], [3], [4]]
// sortIt([4, [1], 3]) ➞ [[1], 3, 4]
// sortIt([[4], 1, [3]]) ➞ [1, [3], [4]]
// sortIt([[3], 4, [2], [5], 1, 6]) // [1, [2], [3], 4, [5], 6]


//Task 35 (array, string)
// function ascDesNone(arr, str) {
// 	if(str==="Asc"){
//     return arr.sort((a,b)=>a-b)
//   }else if(str==="Des"){
//     return arr.sort((a,b)=>b-a)
//   }
//     return arr
// }
// ascDesNone([4, 3, 2, 1], "Asc" ) // [1, 2, 3, 4]
// ascDesNone([7, 8, 11, 66], "Des") // [66, 11, 8, 7]
//  ascDesNone([2, 1, 3, 4], "None") // [1, 2, 3, 4]


// Task 36 (array, reduce)
// function indexMultiplier(arr) {
//   return arr.reduce((acc, curent, index)=>{
// return acc+curent*index
//   },0)
//}
// Another solution
// function indexMultiplier(arr) {
// var result = 0
// for(var i=0; i<arr.length; i++){
//   result+=arr[i]*i
// }
// return result
//}
// indexMultiplier([1, 2, 3, 4, 5]) // 40
// // (1*0 + 2*1 + 3*2 + 4*3 + 5*4)
// indexMultiplier([-3, 0, 8, -6]) // -2
// // (-3*0 + 0*1 + 8*2 + -6*3)



// Task 37 (array)
// function isSpecialArray(arr) {
//   var result
//   var counter =0
// 	for(let i=0; i<arr.length; i+=2){
//     console.log(arr[i+1])
//     if(arr[i]%2===0 && i%2===0 && arr[i+1]%2===1 && (i+1)%2===1){
//       result = true
//     }else{
//       result = false
//       counter++
//     }
//   }
//    console.log(counter)
//    if(counter>0){
//      result=false
//    }
//   return result
// }
// isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]) // true
// Even indices: [2, 4, 6, 6]; Odd indices: [7, 9, 1, 3]
// isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]) // false
// // Index 2 has an odd number 9.
// isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]) // false
// Index 3 has an even number 8.


// Task 38 (array)
// Fix this incorrect code so that all tests pass!
// function flatten(arr) {
// var arr2 = [];
// for (let i = 0; i < arr.length; i++) {
//   console.log(arr[i])
//   // arr2.concat(arr[i]);
//   for(var j=0; j<arr[i].length; j++){
//     arr2.push(arr[i][j])
//   }
// return arr2; 
// }
// }
// Another solution
// function flatten(arr) {
//    var arr2 = [];
//   for (let i = 0; i < arr.length; i++) {
//     arr2=arr2.concat(arr[i])
//   }
//   return arr2; 
// }
// flatten([[1, 2], [3, 4]]) // []
// Expected: [1, 2, 3, 4]
// flatten([["a", "b"], ["c", "d"]]) // []
//  Expected: ["a", "b", "c", "d"]
// flatten([[true, false], [false, false]]) // []
// Expected: [true, false, false, false]




// Task 38 (array, map)
// function jazzify(arr) {
//     if(arr.length===0){
//       return []
//     }
// 	return arr.map(item=>{
//   if(item[item.length-1]==='7'){
//     return item
//   }else{
//          return item+7
//   }
//   })
// }
// jazzify(["G", "F", "C"]) // ["G7", "F7", "C7"]
// jazzify(["Dm", "G", "E", "A"]) // ["Dm7", "G7", "E7", "A7"]
// jazzify(["F7", "E7", "A7", "Ab7", "Gm7", "C7"]) // ["F7", "E7", "A7", "Ab7", "Gm7", "C7"]
// jazzify([]) // []



// Task 39 (array)
// function clone(arr) {
//   var result = arr.push([...arr])
//   console.log(arr)
//   // for(var i=0; i<arr.length;i++){
//   //   console.log(arr[i])
//   //   result.push(arr[i],arr)
//   // }
// 	return result
// }
// clone([1, 1]) // [1, 1, [1, 1]]
// clone([1, 2, 3]) // [1, 2, 3, [1, 2, 3]]
// clone(["x", "y"]) // ["x", "y", ["x", "y"]]



// Task 40 (array)
// function totalVolume(...boxes) {
//   var result =0
//   var arr = [...boxes]
//   console.log(arr)
//   for(var i=0; i<arr.length;i++){
// result += (arr[i][0]*arr[i][1]*arr[i][2])
//   }
// 	return result
// }
// totalVolume([4, 2, 4], [3, 3, 3], [1, 1, 2], [2, 1, 1]) // 63
// totalVolume([2, 2, 2], [2, 1, 1]) // 10
// totalVolume([1, 1, 1]) // 1



// Task 41 (array)
// An array is special if every even index contains an even number and every odd index contains an odd number. 
// Create a function that returns true if an array is special, and false otherwise.
// function isSpecialArray(arr) {
//   let evenFlag =false
//   let oddFlag = false
// 	for(i=0; i<arr.length; i+=2){
//        console.log(arr[i]%2)
//     if(arr[i]%2===0 && i%2===0){
//       evenFlag = true
//     }else{
//       evenFlag=false
//       break
//     }
//   }
//   for(i=1; i<arr.length;i+=2){
//     if(arr[i]%2===1 && i%2===1){
//       oddFlag=true
//     }else{
//       oddFlag=false
//       break
//     }
//   }
//   if(evenFlag&& oddFlag){
//     return true
//   }
//   return false
// }
// isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]) // true
// Even indices: [2, 4, 6, 6]; Odd indices: [7, 9, 1, 3]
// isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]) // false
// Index 2 has an odd number 9.
// isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]) // false
// Index 3 has an even number 8.



// Task 42(array)
// function numbersSum(arr) {
//   // var result=0
// 	// for(var i=0; i<arr.length; i++){
//   //   if(typeof arr[i]==='number'){
//   //     result+=arr[i]
//   //   }
//   // }
//   // return result
//   return arr.reduce((acc, curent)=>{
//     if(typeof curent==='number'){
//       return acc+curent
//     }
//     return acc
//   },0)
// }
// numbersSum([1, 2, "13", "4", "645"]) // 3
// numbersSum([true, false, "123", "75"]) // 0
// numbersSum([1, 2, 3, 4, 5, true]) // 15



// Task 43 (array)
// function marathonDistance(d) {
//   var distance = d.reduce((acc, curent)=>{
// return acc+Math.abs(curent)
//   },0)
//   return(distance===25) ? true:false
// }
// marathonDistance([1, 2, 3, 4]) // false
// marathonDistance([1, 9, 5, 8, 2]) // true
// marathonDistance([-6, 15, 4]) // true



// Task 44 (array, flag)
// function allTruthy(...args) {
// 	console.log(args)
//   var flag = true
//   for(var i=0; i<args.length; i++){
//     var bool = !!args[i]
//     console.log(typeof args[i])
//     if(bool===false){
//       flag=false
//     break
//     }
//   }
//   return flag
// }
// allTruthy(true, true, true) // true
// allTruthy(true, false, true) //false
// allTruthy(5, 4, 3, 2, 1, 0) //false
// allTruthy(undefined, true, true) // 



// Task 45 (array)
// function factorChain(arr) {
//   for(var i=1; i<arr.length; i++){
//     if(arr[i]%arr[i-1]!==0){
//       return false
//     }
//   }
//   return true
// }
// factorChain([1, 2, 4, 8, 16, 32]) // true
// factorChain([1, 1, 1, 1, 1, 1]) //true
// factorChain([2, 4, 6, 7, 12]) // false
// factorChain([10, 1]) // false



// Task 46 (array)
// function progressDays(runs) {
//   var result=0
// 	for(var i=1; i<runs.length;i++){
//     if(runs[i]>runs[i-1]){
//       result++
//     }
//   }
//   return result
// }
// progressDays([3, 4, 1, 2]) //2
//  There are two progress days, (3->4) and (1->2)
// progressDays([10, 11, 12, 9, 10]) // 3
// progressDays([6, 5, 4, 3, 2, 9]) // 1
// progressDays([9, 9])  // 0



// Task 47 (array)
// function uniqueSort(arr) {
// var sorted = arr.sort((a,b)=>{
//  return a-b
// })
// console.log(sorted)
// var result =[]
// for(var i=0; i< sorted.length;i++){
//   if(sorted[i]!==sorted[i+1]){
//     result.push(sorted[i])
//   }
// }
// return result
// }
// Another solution 
// function uniqueSort(arr) {
// const newArr =new Set(arr)
// return [...newArr].sort((a,b)=>a-b)
//   return [... new Set(arr)].sort((a,b)=>a-b)
// }
//  uniqueSort([1, 2, 4, 3]) // [1, 2, 3, 4]
// uniqueSort([1, 4, 4, 4, 4, 4, 3, 2, 1, 2]) // [1, 2, 3, 4]
// uniqueSort([6, 7, 3, 2, 1]) // [1, 2, 3, 6, 7]


// Task 48 (array)
// function zipIt(women, men) {
// 	if(women.length!==men.length){
//     return "sizes don't match"
//   }
//   var result = []
//   for(var i=0; i<women.length; i++){
//       result.push([women[i], men[i]])
//   }
//   return result
// }
// zipIt(["Elise", "Mary"], ["John", "Rick"])
// [["Elise", "John"], ["Mary", "Rick"]]
// zipIt(["Ana", "Amy", "Lisa"], ["Bob", "Josh"])
//  "sizes don't match"
// zipIt(["Ana", "Amy", "Lisa"], ["Bob", "Josh", "Tim"])
//  [["Ana", "Bob"], ["Amy", "Josh"],["Lisa", "Tim"]]



// Task 49 (array, Math)
// function lineLength([x1, y1], [x2, y2]) {
//   return (Math.sqrt((x2-x1)**2+(y2-y1)**2)).toFixed(2)
// }
// lineLength([15, 7], [22, 11]) // 8.06
// lineLength([0, 0], [0, 0]) // 0
// lineLength([0, 0], [1, 1]) // 1.41


// Task 50 (string)
// In order to work properly, 
//the function should replace all "a"s with 4, "e"s with 3, "i"s with 1, "o"s with 0, and "s"s with 5.
// function hackerSpeak(str) {
//   var result = ''
//   for (var i =0; i<str.length; i++){
// if(str[i]==='a'){
//   result+=4
// }else if(str[i]==='e'){
//   result+=3
// }else if(str[i]==='i'){
//   result+=1
//   }else if(str[i]==='o'){
//   result+=0
//   }else if(str[i]==='s'){
//   result+=5
//   }else{
//     result+=str[i]
//   }
//   }
//   return result
// }
// hackerSpeak("javascript is cool") // "j4v45cr1pt 15 c00l"
// hackerSpeak("programming is fun") // "pr0gr4mm1ng 15 fun"
// hackerSpeak("become a coder") // "b3c0m3 4 c0d3r"



// Task 51 (array)
// function capMe(arr) {
//   var arr1 = []
//   let result = ""
// 	for(var i=0; i<arr.length; i++){
// result=arr[i][0].toUpperCase()+arr[i].slice(1).toLowerCase()
// arr1.push(result)
//   }
//   return arr1
// }
// Another solution 
// function capMe(arr) {
// return arr.map(item=>{
//  return item[0].toUpperCase() + item.slice(1).toLowerCase()
// })
// }
// capMe(["mavis", "senaida", "letty"]) // ["Mavis", "Senaida", "Letty"]
// capMe(["samuel", "MABELLE", "letitia", "meridith"]) // ["Samuel", "Mabelle", "Letitia", "Meridith"]
// capMe(["Slyvia", "Kristal", "Sharilyn", "Calista"]) // ["Slyvia", "Kristal", "Sharilyn", "Calista"]



// Task 52 (array)
// function sumTwoSmallestNums(arr) {
//   var result = []
// 	var sorted = arr.sort((a,b)=>{
//     return a-b
//   })
//   for(i=0; i<sorted.length;i++){
//     if(sorted[i]>0){
// result = sorted.splice(i,2)
// break
//     }
//   }
//   return result.reduce((acc, curent)=>acc+curent)
// }
// sumTwoSmallestNums([19, 5, 42, 2, 77]) // 7
// sumTwoSmallestNums([10, 343445353, 3453445, 3453545353453]) //3453455
// sumTwoSmallestNums([2, 9, 6, -1]) // 8
// sumTwoSmallestNums([879, 953, 694, -847, 342, 221, -91, -723, 791, -587]) // 563
// sumTwoSmallestNums([3683, 2902, 3951, -475, 1617, -2385]) // 4519


// Task 53 (array)
// function mirror(arr) {
//   var newArr = [...arr]
// 	var reversed = newArr.reverse().shift()
//   return arr.concat(newArr)
// }
// mirror([0, 2, 4, 6]) // [0, 2, 4, 6, 4, 2, 0]
// mirror([1, 2, 3, 4, 5]) // [1, 2, 3, 4, 5, 4, 3, 2, 1]
// mirror([3, 5, 6, 7, 8]) // [3, 5, 6, 7, 8, 7, 6, 5, 3]



// Task 54 (array)
// function reverseArr(num) {
// return String(num).split("").reverse().map(item=>Number(item))
// }
// reverseArr(1485979) // [9, 7, 9, 5, 8, 4, 1]
// reverseArr(623478) // [8, 7, 4, 3, 2, 6]
// reverseArr(12345) // [5, 4, 3, 2, 1]


// Task 55 (array)
// function warOfNumbers(arr) {
// 	var sumOdd = arr.filter(item=>item%2===1).reduce((acc,curent)=>acc+curent)
//   var sumEven = arr.filter(item=>item%2===0).reduce((acc,curent)=>acc+curent)
//   return Math.abs(sumOdd-sumEven)
//  for(var i=0; i<arr.length; i++){
//   if(arr[i]%2===0){
//     sumEven+=arr[i]
//   }else{
//     sumOdd+=arr[i]
//   }
// }
// if(sumEven>sumOdd){
//   result= sumEven - sumOdd
// }else{
//   result = sumOdd-sumEven
// }
// return result
//}
// warOfNumbers([2, 8, 7, 5]) // 2
// 2 + 8 = 10
// 7 + 5 = 12
// 12 is larger than 10
// So we return 12 - 10 = 2
// warOfNumbers([12, 90, 75]) // 27
// warOfNumbers([5, 9, 45, 6, 2, 7, 34, 8, 6, 90, 5, 243]) // 168


// Task 55 (object)
// function createObj(name, year, producer){
//     var object = {}
//     object['name']= name
//     object['year'] = year
//     object['producer']= producer
//     return object
//   }
// another solution
// function createObj(name, year, producer){
//   return {name, year, producer}
//   }
//   createObj("Джон Уик", 2011, 'Киану Ривз')


// Task 56 (object)
// Напишите функцию полного копирования свойств объекта. 
// Все значения свойств - примитивные.
// Функция в качестве аргумента получает объект и возвращает новый объект.
// const card1 = {
//    title: "Джон Сильвер",
//    age: 42
// };
// function copyObj(object){
//   return {...object}
// }
// copyObj(card1)
// Another solution 
// function copyObj(object){
//     var copy = Object.assign({},object)
// return copy
//   }
//another solution 
// function copyObj(object){
//     var copy = {}
//     for(key in object){
//       copy[key]=object[key]
//     }
//     return copy
//   }



// Task 56 (object)
// Напишите функцию, сравнивающую два объекта по свойствам. 
// Если у объектов полностью совпадают названия 
// свойств и их значения - функция возвращает true. Если нет - false.
//     const pirate1 = {
//         title: "Джон Сильвер",
//         age: 41,
//     };
//     const pirate2 = {
//         title: "Джон Сильвер",
//         age: 41
//     };
//     function camparingObj(obj1, obj2){
// for (key in obj1){
// if(!obj2[key] || obj2[key]!==obj1[key]){
// return false
// }
// }
// return true
//     }
//     camparingObj(pirate1, pirate2)
// Another solution
// function camparingObj(obj1, obj2){
//     var string1 = JSON.stringify(obj1)
//     var string2 =JSON.stringify(obj2)
//     return (string1===string2)? true:false
//             }
//             camparingObj(pirate1, pirate2)


// Task 56 (object)
// Напишите функцию getShortProductInfo, 
//   которая получает объект типа product, 
//   и возвращает из него объект, описывающий краткое описание товара.  
//   const product = {
//     name: "Lenovo X120S",
//     fullName: "X120S (233-544-2)",
//     price: 18600,
//     shortDescr: "",
//     fullDesr: ""
// };
// function getShortProductInfo(obj, string){
//   var result ={}
//   for(key in obj){
//     result[string]= obj[key]
//   }
//   return result
// }
// getShortProductInfo(product, "shortDescr")
// Another solution
// function getShortProductInfo(obj, string,string1){
//     var result ={}
//         result[string1] = obj[string1]
//       result[string]= obj[string]
//     return result
//   }
//   getShortProductInfo(product, "shortDescr", "price")
// opposite solution
// function getShortProductInfo(obj, string,string1){
//     var result = Object.assign({}, obj)
//     for(i=1; i<arguments.length; i++){
//       console.log(arguments[i])
//       const key = arguments[i]
//       delete result[key]
//     }
//     return result
//      }
//      getShortProductInfo(product, "shortDescr", "price")



// Task 57 (object, methods)
//         Допишите объекту product метод getReviewByDate.
//         Метод получает дату искомого отзыва, находит его в массиве reviews объекта,
//         и возвращает объект, описывающий нужны отзыв.
//         */
//         const product = {
//             name: "Lenovo X120S",
//             price: 12000,
//             reviews: [{
//                 date: "12.09.2019",
//                 text: "Хорошо работает как подставка для кофе",
//                 userName: "Alex"
//             }, {
//                 date: "03.15.2020",
//                 text: "Еще на нем отлично можно жарить яичницу",
//                 userName: "Bragin"
//             }],
//             getReviewByDate(date){
// var result = this.reviews.find(review=>review['date']===date)
//            return result
//             }
//             }
//         console.log(product.getReviewByDate("03.15.2020"));



// Task 57 (object, methods)
// напишите функцию, Которая выводит объект product в виде карточки товара на экран. 
// HTML-разметку карточки придумайте сами
//     const product =[ {
//         name: "Lenovo X120S",
//         fullName: "X120S (233-544-2)",
//         price: 18600,
//         shortDescr: "super",
//         fullDesr: ""
//     }];
//     function render(product){
// const wrapper = document.getElementById("root")
// product.forEach(item=>{
// const div = document.createElement("div")
// div.insertAdjacentHTML("beforeend", `<h2>${item.name}</h2> <p>${item.fullName}</p> <p>${item.price}</p> <p>${item.shortDescr}</p>` )
// wrapper.appendChild(div)
// })
//     }
//     render(product)


// Task 57 (object, destruct)
// let names = []
// let users = [
//   { name: "John", email: "john@example.com" },
//   { name: "Jason", email: "jason@example.com" },
//   { name: "Jeremy", email: "jeremy@example.com" },
//   { name: "Jacob", email: "jacob@example.com" }
// ]
// for(let {name} of users) {
//       names.push(name)
// }
// console.log(names) // should log ["John", "Jason", "Jeremy", "Jacob"]



// Task 58 (array, destruct)
//  Две компании решили объединиться, и для этого им нужно объединить базу данных своих клиентов.
//  У вас есть 2 массива строк, в каждом из них - фамилии клиентов. Создайте на их основе один массив, 
// который будет представлять собой объединение двух массив без повторяющихся фамилий клиентов.
//  ```javascript
// const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
// function lastNames(arr1, arr2) {
//     var result = [...new Set([...arr1, ...arr2])]
//     return result
// }
//     lastNames(clients1, clients2)
//another solution
// function lastNames (arr1, arr2){
//     var concated = arr1.concat(arr2)
//     var result = []
//     concated.forEach(item=>{
//        if(!result.includes(item)){
//          result.push(item)
//        }
//     })
//     return result
//          }     
//          lastNames (clients1, clients2)
//another solution
// function lastNames (concated){
//     var result = []
//     concated.forEach(item=>{
//        if(!result.includes(item)){
//          result.push(item)
//        }
//     })
//     return result
//          }     
//          lastNames ([...clients1, ...clients2])



// Task 59 (array, destruct)
// Перед вами массив `characters`, состоящий из объектов. Каждый объект описывает одного персонажа.
// Создайте на его основе массив `charactersShortInfo`, состоящий из объектов, в которых есть только 3 поля - name,
// lastName и age.
// ```javascript
// const characters = [
//     {
//         name: "Елена",
//         lastName: "Гилберт",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Кэролайн",
//         lastName: "Форбс",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Аларик",
//         lastName: "Зальцман",
//         age: 31,
//         gender: "man",
//         status: "human"
//     },
//     {
//         name: "Дэймон",
//         lastName: "Сальваторе",
//         age: 156,
//         gender: "man",
//         status: "vampire"
//     },
//     {
//         name: "Ребекка",
//         lastName: "Майклсон",
//         age: 1089,
//         gender: "woman",
//         status: "vempire"
//     },
//     {
//         name: "Клаус",
//         lastName: "Майклсон",
//         age: 1093,
//         gender: "man",
//         status: "vampire"
//     }
// ];
// function getInfo(arr) {
//     var result = []
//     for (i = 0; i < arr.length; i++) {
//         var person = arr[i]
//         var obj = {}
//         for (key in person) {
//             console.log(person[key])
//             if (key === 'name' || key === 'lastName' || key === 'age') {

//                 obj[key] = person[key]
//             }
//         }
//         result.push(obj)
//     }
//     return result
// }
// getInfo(characters)
//another solution
// function getInfo(arr){
//     var result = []
//    arr.forEach(item=>{
// var {gender, status,...rest}=item
// result.push(rest)
//    })
// return result
//  }
//  getInfo(characters)
// another solution
// function getInfo(arr){
//     return arr.map(item=>{
//       var{gender, status, ...rest}=item
//       return rest
//       })
//  }
//  getInfo(characters)




// Task 60 (array, destruct)
// У нас есть объект `user`:
// ```javascript
// const user1 = {
//  name: "John",
//   years: 30
// };
// ```
// Напишите деструктурирующее присваивание, которое:
// - свойство name присвоит в переменную name
// - свойство years присвоит в переменную age
// - свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
// Выведите переменные на экран.
// const user1 = {
//     name: "John",
//     years: 30,
//     isAdmin: true
// };
// const { name, years: age, isAdmin = false } = user1
// console.log(name, age, isAdmin)
// diferent solution
// function getInfo(obj){
//     var name = obj['name']
//     var age = obj['years']
// var result = {name, age, isAdmin:false}
// return result
//   }
//   getInfo(user1)




// Task 61 (obj, destruct)
// Детективное агентство несколько лет собирает информацию о возможной личности [Сатоши Накамото]
// (https://ru.wikipedia.org/wiki/%D0%A1%D0%B0%D1%82%D0%BE%D1%81%D0%B8_%D0%9D%D0%B0%D0%BA%D0%B0%D0%BC%D0%BE%D1%82%D0%BE).
// Вся информация, собранная в конкретном году, хранится в отдельном объекте.
// Всего таких объектов три - `satoshi2018`, `satoshi2019`, `satoshi2020`.
// Чтобы составить полную картину и профиль, вам необходимо объединить данные из этих трех объектов в один объект -
// `fullProfile`.
// Учтите, что некоторые поля в объектах могут повторяться. В таком случае в результирующем объекте должно
// сохраниться значение, которое было получено позже (например, значение из 2020 более приоритетное по сравнению с 2019).
// Напишите код, который составит полное досье о возможной личности Сатоши Накамото.
// Изменять объекты `satoshi2018`, `satoshi2019`, `satoshi2020` нельзя.
// ```javascript
//     const satoshi2020 = {
//         name: 'Nick',
//         surname: 'Sabo',
//         age: 51,
//         country: 'Japan',
//         birth: '1979-08-21',
//         location: {
//           lat: 38.869422,
//           lng: 139.876632
//         }
//       }
//       const satoshi2019 = {
//         name: 'Dorian',
//         surname: 'Nakamoto',
//         age: 44,
//         hidden: true,
//         country: 'USA',
//         wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//         browser: 'Chrome'
//       }
//       const satoshi2018 = {
//         name: 'Satoshi',
//         surname: 'Nakamoto',
//         technology: 'Bitcoin',
//         country: 'Japan',
//         browser: 'Tor',
//         birth: '1975-04-05'
//       }
//   var newObj = {...satoshi2018,
//                 ...satoshi2019,
//                  ...satoshi2020,
//                 }
//                 console.log(newObj)




// Task 62 (obj, destruct)
// Дан массив книг. Вам нужно добавить в него еще одну книгу, не изменяя существующий массив
// (в результате операции должен быть создан новый массив).
// ```javascript
// const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
//   }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
//   }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
//   }];

//   const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
//   }
//   var result = [...books, bookToAdd ]
//   console.log(result)
// another solution
//function addProperty(arr, obj){
// var result=[]
// for(i=0; i<arr.length; i++){
//   var book = arr[i]
//   result.push(book)
// }
// result.push(obj)
// return result
//     }
//     addProperty(books, bookToAdd)



// Task 63 (obj, destruct)
// Дан обьект `employee`. Добавьте в него свойства age и salary, не изменяя изначальный объект (должен быть
// создан новый объект, который будет включать все необходимые свойства). 
//Выведите новосозданный объект в консоль.
// ```javascript
// const employee = {
//   name: 'Vitalii',
//   surname: 'Klichko'
// }
// function addProperty(obj) {
//     var result = {}
//     result['age'] = 45
//     result['salary'] = 10000
//     for (key in obj) {
//         result[key] = obj[key]
//     }
//     return result
// }
// addProperty(employee)
// // another solution
// function addProperty(obj) {
//     return { ...obj, age: 45, salary: 10000 }
// }
// addProperty(employee)


// Task 64 (array, destruct)
// Дополните код так, чтоб он был рабочим
// ```javascript
// const array = ['value', () => 'showValue'];
// Допишите ваш код здесь
// alert(value); // должно быть выведено 'value'
// alert(showValue());  // должно быть выведено 'showValue'
// ```
// const array = ['value', () => 'showValue'];
// var [value, showValue]= array
//  console.log(value); // должно быть выведено 'value'
//         console.log(showValue());  // должно быть выведено 'showValue'



// Task 65 (string, array, object)
// given string with data and string for sort. Change string to array of objects
// const string = "city,temp2,temp\nParis,7,-3\nDubai,4,-4\nPorto,-1,-2\nSanFrancisco,15,7";
// const string1 = "temp";
// function sortTemps(S, C) {
//     var splitted = S.split('\n')
//     console.log(splitted)
//     var array = splitted.map(item => {
//         return item.split(',')
//     })
//     var result = rest.map(item => {
//         var obj = {}
//         item.forEach((item, index) => {
//             obj[field[index]] = item
//         })
//         return obj
//     })
//     console.log(result)
//     var sorted = result.sort((a, b) => {
//         return b[C] - a[C]
//     })
//     return +sorted[0][C]
// }
// sortTemps(string, string1)



// Task 66 (array, object)
// var temperatures=[{city:"New York", temperature:4}, {city:"Vladivostok", temperature:12}, {city:"Tokyo", temperature:20}, {city:"Paris", temperature:15}, {city:"London", temperature:7}]
// function getTemperature(temperatures){
// var coldest = 0
// var hotest = 0
// var cityColdest = ""
// var cityHotest = ""
// for (var i=0; i<temperatures.length; i++){
//   var city = temperatures[i]
//   if(city['temperature']<coldest)
// }
//   var result = {coldest, hotest} 
// }
// getTemperature(temperatures)
//another solution
//function getTemperature(temperatures){
//    var coldest = 0
//   var hotest = 0
//   var sorted = temperatures.sort((a,b)=>{
//     return a['temperature']- b['temperature']
//   })
//   coldest = sorted[0].city
//   hotest = sorted[sorted.length-1].city
//   var result = {coldest, hotest} 
// }
// getTemperature(temperatures)



// Task 67 (array)
// var array = [2, 3, 4, 5, 6]
// var item =5
// function indexOfArray(array, item) {
//   if(array.indexOf(item)=== undefined){
//       return -1
//   }else{
//   return array.indexOf(item)
//   }
// }
// indexOfArray(array, item)




// Task 68 (string, object)
// function countAllCharacters(str) {
//   var obj={}
//   for(i=0; i<str.length; i++){
//     if(str[i] in obj){
//     obj[str[i]]++
//     }else{
//     obj[str[i]]=1
//     }
//   }
//   return obj
// }
// var output = countAllCharacters('banana');
// console.log(output); // --> {b: 1, a: 3, n: 2}



// Task 69 (string, object)
//  Создать функцию findMostPopular()
//  Функция должна принимать массив в виде аргумента и возвращать самый
//  часто повторяющийся в нем элемент.
//  Условие гарантирует что такой элемент будет один.
// console.log(findMostPopular([1, 2, 3, 2, 1, 3, 3, 4])) // 3
// console.log(findMostPopular(['z', 12, 'z', 'dd', 13, 10])) // 'z'
// function findMostPopular(array) {
//     var obj = {}
//     var highest = {
//         key: "",
//         count: 0
//     }
//     for (var i = 0; i < array.length; i++) {
//         if (array[i] in obj) {
//             obj[array[i]]++
//         } else {
//             obj[array[i]] = 1
//         }
//     }
//     for (var key in obj) {
//         if (highest['count'] < obj[key]) {
//             highest['count'] = obj[key]
//             highest['key'] = key
//         }
//     }
//     return highest["key"]
// }



// Task 70 (array)
// Write a function called 'checkWinner'. This function will take an array with a length of 7.
// Each element of the array will be either; 'red', 'black', or 0.
// We can assume that no violation of either of these is possible (i.e. input will always be of length 7,
// and elements will only be 'red', 'black', or 0).
// Your function should accept this array as its only parameter.
// If there are 4 'red' elements consecutively in a row, 'checkWinner' should return the string:
// 'Red Wins!'. Similarly, if there are 4 'black' elements consecutively in a row, 
//'checkWinner' should return the string: 'Black Wins!'. 
//If neither of these is the case, 'checkWinner' should return 'Draw!'.
// Here are some examples of your code running, assuming you have successfully created the described function.
// Please be sure to name the function "checkWinner".
// let blackWinner = checkWinner(['black', 'red', 'black', 'black', 'black', 'black', 'red']);
// console.log(blackWinner); //-> 'Black Wins!'
// let redWinner = checkWinner([0, 0, 0, 0, 'red', 'red', 'red']);
// console.log(redWinner); //-> 'Red Wins!'
// let draw = checkWinner(['red', 'red', 'red', 'black', 'black', 'black', 0]);
// console.log(draw); // -> 'Draw!'
// function checkWinner(array) {
//     let red = 0
//     let black = 0
//     let zero = 0
//     let winner = ""
//     array.forEach(item => {
//         if (item === 'red') {
//             red += 1
//         } else if (item === 'black') {
//             black += 1
//         } else {
//             zero += 1
//         }
//     })
//     if (red > black && red > zero) {
//         winner = 'Red Wins'
//     } else if (black > red && black > zero) {
//         winner = "Black Wins"
//     } else if (zero > red && zero > black) {
//         winner = 'Zero Wins'
//     } else {
//         winner = "Draw"
//     }
//     return winner
// }



// Task 71 (array)
// Binary search is a technique for very rapidly searching a sorted collection by 
//cutting the search space in half at every pass.
// Given a sorted array, such as this:
// [1, 3, 16, 22, 31, 33, 34]
// You can search for the value 31, as follows:
// 1) Pick the midpoint: 22.
// 2) The value is higher than 22, so now consider only the right half of the previous array:
// [...31, 33, 34]
// 3) Pick the midpoint: 33
// 4) The value is lower than 33, so now consider only the left half of the previous array:
// [...31...]
// 5) Pick the midpoint: 31
// 6) You've hit the value.
// 7) Return the index of the value.
// Notes:
// * If you don't find the value, you can return null.
// * If at any point you calculate the index of the midpoint and get a fractional number, just round it down ("floor" it).
// const array = [1, 3, 16, 22, 31, 33, 34]
// const number = 3
// function binerySearch(array, number){
//   let first = 0
//   let last =array.length-1
//   let position = null
//   let found = false
//   let middle 
//   while(found===false && first<= last){
//     middle = Math.floor((first+last)/2)
//     if(array[middle]===number){
//       found = true
//       position = middle
//     }else if(array[middle]>number){
//       last = middle-1
//     }else{
//       first=middle+1
//     }
//     return position
//  }
// let midPoint =Math.floor((array.length)/2)
// if(array[midPoint]<number){
//   const newArray = array.slice(midPoint)
//  binerySearch(newArray, number)
// }else if(array[midPoint]>number){
//   const newArray = array.slice(0,midPoint)
//   //console.log(midPoint)
//  binerySearch(newArray, number)
// }else if(array[midPoint]===number) {
//   return "find"
// }else{
//   return null
// }
// }
// binerySearch(array, number)
//another solution
// const array = [1, 3, 16, 22, 31, 33, 34]
// const number = 22
// let position = null
// function binerySearch(array, number){
//   let first = 0
//   let last =array.length-1
//   let found = false
// let middle 
//   while(found===false && first<= last){
//     middle = Math.floor(( first + last )/2)
//     if(array[middle]==number){
//       found = true
//       position = middle
//     }else if(array[middle]>number){
//       last = middle - 1
//     }else{
//       first = middle + 1
//     }
//   }
// }
// binerySearch(array, number)



// Task 72 (loop, while)
// // Выведите в консоль числа от 1 до 100.
// let num = 1
// while(num>=1 && num<=100){
//   console.log(num++)
// }



// Task 72_2 (loop, while)
// // Выведите в консоль числа от 11 до 33.
// let num =
// while(num>=11 && num<=33){
//   console.log(num++)
// }


// Task 73 (loop, while)
// // Выведите в консоль четные числа в промежутке от 0 до 100.
// let num = 0
// while(num<=100){
//   if(num%2===1){
//   console.log(num)
//   }
//   num++
// }



// Task 74 (loop, while)
// Выведите в консоль числа от 30 до 0.
// let num = 30
// while(num<=30 && num>=0){
//   console.log(num--)
// }



// Task 75 (loop, while)
// // В следующем коде программист вывел числа от 10 до 1:
// let i = 10;
// while (i >= 1 && i <=10) {
// 	console.log(i--);
// }



// Task 76 (loop, while)
// В коде, однако, была допущена ошибка, которая привела к тому, что цикл выполняется бесконечно. 
//Исправьте ошибку программиста.
// В следующем коде программист вывел числа от 10 до 1:
// let i = 10;
// while (i >= 1 && i<=10) {
// 	console.log(i);
// 	i--;
// }



// Task 77 (loop, while)
// В коде, однако, была допущена ошибка, которая привела к тому, что цикл выполняется бесконечно.
// Исправьте ошибку программиста.
// В следующем коде программист вывел числа от 10 до 1:
// let i = 10;
// while (i >= 1 && i<=10) {
// 	console.log(i);
// 	i--;
// }



// Task 78 (loop, while)
// В коде, однако, была допущена ошибка, которая привела к тому, что на экран ничего не вывелось.
// Исправьте ошибку программиста.
// В следующем коде программист вывел числа от 10 до 1:
// let i = 10;
// while (i >= 1 ) {
// 	console.log(i);
// 	i--;
// }


// Task 79 (loop, while)
// В коде, однако, была допущена ошибка, которая привела к тому, что на экран ничего не вывелось.
// Исправьте ошибку программиста.
// В следующем коде программист вывел числа от 10 до 1:
// let i = 10;
// while (i >= 1) {
// 	console.log(i);
// 	i--;
// }



// Task 80 (array)
// You're fixing an Electrocardiogram program that captures heart-rates. 
//The problem is that it doesn't indicate whether the rates are normal or bad rhythms (Arrhythmic).
// If a heart-rate is less that 60 beats-per-minute, we say that it is Bradycardic, latin for 'slow heart'.
// If the heart-rate is greater than 100, we say that it is Tachycardic, 
//also latin meaning 'fast heart'. Normal heart rates are in between.
// Your job is to fix the diagnoseRates function so that it can take an array of heart-rates and
// return a new array where each rate is formatted into a string with the cooresponding diagnosis along with the rate.
// For example, the data [ 42, 77, 103 ] would return ["Bradycardia: 42", "Normal: 77", "Tachycardia: 103"]. 
//See if you can fix diagnoseRates and help improve patient outcomes!
// const data = [ 42, 77, 103 ]
// function diagnoseRates(array){
//   const result= []
// for (let i =0 ; i <array.length; i++){
//   if(array[i]<60){
//     result.push( "Bradycardia: " +array[i])
//   }else if(array[i]>=60 && array[i]<=100){
//     result.push( "Normal: " +array[i] )
//   }else {
//     result.push( `Tachycardia: ${array[i]}`)
//   }
// }
// return result
// }
// another solution
// function diagnoseRates(array){
// return array.map(item => {
//     if (item < 60) {
//         return `Bradycardia: ${item}`
//     } else if (item >= 60 && item <= 100) {
//         return `Normal: ${item}`
//     } else {
//         return `Tachycardia: ${item}`
//     }
// })

// }
// diagnoseRates(data)




// Task 81 (array)
// // Write a function that when given a URL as a string,
// parses out just the domain name and returns it as a string. For example:
// Input1: "http://github.com/carbonfive/raygun  " Output1: "github"
// Input2: "http://www.zombie-bites.com  " Output2: "zombie-bites"
// Input3: "https://www.cnet.com  " Output3: "cnet"
// const string ="http://github.com/carbonfive/raygun"  
// const string1 ="http://www.zombie-bites.com  "
// function getDomain(string){
//   let newString =''
//   if(string.includes('www')){
//     newString= string.substring(string.indexOf('.')+1, string.lastIndexOf('.'))
//   }else{
//     newString= string.substring(string.indexOf('//')+2, string.lastIndexOf('.'))
//   }
// return newString
//}
// getDomain(string1)
// another solution
// function getDomain(string){
// for (let i=0 ; i<string.length; i++){
// if (string[i]==="/" && string[i]==='.'){
// newString = string.slice(i)
// console.log(newString)
// }
// }
// // const array = string.split('//')
// // console.log(array)
// return newString
// }
// getDomain(string)




// Task 82 (array, object)
// function you will write will be called generateSampleView.
// The input for this function will always be an array of objects, 
// theoretically the result of a call to an API, or database.
// generateSampleView will take this array as its parameter, 
// and return an array of strings based upon conditions that we will describe in a moment. 
// Note: your function will be tested with a longer input,
// but the format will remain consistent for each user in the input array.
// Your function should examine each user object, and add to the array return value for 
//this function one of the following:
// If the value of the id property is odd, add the user's email to the return array
// If the value of the id property is even, your function should create a new string for the given user,
// add the street, suite, city, and zipcode, each separated by a space and a comma, as one string,
// to the return array
// Thus, if our input was the users array listed above, our output would be:
// var output = ["Sincere@april.biz", "Victor Plains, Suite 879,  Wisokyburgh, 90566-7771"];
// The format of this input array of objects is described below:
// var users = [
//   {
//     "id": 1,
//     "name": "Leanne Graham",
//     "username": "Bret",
//     "email": "Sincere@april.biz",
//     "address": {
//       "street": "Kulas Light",
//       "suite": "Apt. 556",
//       "city": "Gwenborough",
//       "zipcode": "92998-3874",
//       "geo": {
//         "lat": "-37.3159",
//         "lng": "81.1496"
//       }
//     },
//     "phone": "1-770-736-8031 x56442",
//     "website": "hildegard.org",
//     "company": {
//       "name": "Romaguera-Crona",
//       "catchPhrase": "Multi-layered client-server neural-net",
//       "bs": "harness real-time e-markets"
//     }
//   },
//   {
//     "id": 2,
//     "name": "Ervin Howell",
//     "username": "Antonette",
//     "email": "Shanna@melissa.tv",
//     "address": {
//       "street": "Victor Plains",
//       "suite": "Suite 879",
//       "city": "Wisokyburgh",
//       "zipcode": "90566-7771",
//       "geo": {
//         "lat": "-43.9509",
//         "lng": "-34.4618"
//       }
//     },
//     "phone": "010-692-6593 x09125",
//     "website": "anastasia.net",
//     "company": {
//       "name": "Deckow-Crist",
//       "catchPhrase": "Proactive didactic contingency",
//       "bs": "synergize scalable supply-chains"
//     }
//   }
// ];
// function generateSampleView(users){
//   let array =[]
//   for(let i =0; i<users.length;i++){
//     if(users[i].id%2===0){
//       array.push( `${users[i]["address"]["street"]}, ${users[i]["address"]["suite"]}, ${users[i]["address"]["city"]}, ${users[i]["address"]["zipcode"]}`)
//     }else{
//       array.push( `${users[i]["email"]}`)
//     }
//   }
//  return array
//}
//another solution
// function generateSampleView(users){
// const result = users.map(item=>{
// if(item.id%2===0){
// return `${item.address.street}, ${item.address.suite}, ${item.address.city}, ${item.address.zipcode} `
//  }else{
//    return item.email
//  }
// })
// return result
// }
// generateSampleView(users)




// Task 83 (string)
// Flip every chunk of n characters in a string, where n is any positive integer greater than 1.
// Note that this is intentionally very similar to the previous problem.
// Please focus on getting a working solution with the tools you know well.
// Practice the interactive/collaborative interview style that's described in the documentation.
// Example:
// var input = 'a short example';
// var output = flipEveryNChars(input, 5);
// console.log(output); // --> ohs axe trelpma
// Breaking this example down piece by piece:
// 'a sho' -> 'ohs a'
// 'rt ex' -> 'xe tr'
// 'ample' -> 'elpma'
// -> 'ohs axe trelpma'
// var input = 'a short example';
// var output = flipEveryNChars(input, 5);
// const a = 1
// function flipEveryNChars(input, n) {
//     const a = 2
//     const b = 2
//     let flipped = ""
//     const characters = input.split('')
//     for (let i = 0; i < characters.length; i += n) {
//         const a = 3
//         const c = 4
//         const currentSlice = characters.slice(i, i + n)
//         const reversed = currentSlice.reverse()
//         let joinedSlice = reversed.join('')
//         flipped += joinedSlice
//     }
//     return flipped
// }
// flipEveryNChars(input, 5)


// Task 84 (object, string)
// Given a string of words, you need to find the highest scoring word.
// Each letter of a word scores points according to its position in the alphabet: a = 1, b = 2, c = 3 etc.
// You need to return the highest scoring word as a string.
// If two words score the same, return the word that appears earliest in the original string.
// All letters will be lowercase and all inputs will be valid.
// const string = "I love coding"
// const object = {
//     a: 1,
//     b: 2,
//     c: 3,
//     d: 4,
//     e: 5,
//     f: 6,
//     g: 7,
//     h: 8,
//     i: 9,
//     j: 10,
//     k: 11,
//     l: 12,
//     m: 13,
//     n: 14,
//     o: 15,
//     p: 16,
//     q: 17,
//     r: 18,
//     s: 19,
//     t: 20,
//     u: 21,
//     v: 22,
//     w: 23,
//     x: 24,
//     y: 25,
//     z: 26,
// }
// function highestScoringWord(string, object) {
//     const newString = string.toLowerCase()
//     const words = newString.split(' ')
//     const scores = []
//     words.forEach(item => {
//         console.log(item)
//         if (item.includes(item in object)) {

//             scores.push(object[item])
//         }
//     })
//     console.log(scores)
//     let firstWord = {}
//     words.forEach(item => {
//         firstWord[item] = scores.slice(0, item.length)
//     })
//     const finalScore = {}
//     for (let key in firstWord) {
//         firstWord[key].reduce((acc, curent) => {
//             return finalScore[key] = acc + curent
//         }, 0)
//     }
//     const newArray = Object.values(finalScore)
//     const sorted = newArray.sort((a, b) => {
//         return b - a
//     })
//     let result = sorted[0]
//     let resultWord = null
//     for (let key in finalScore) {
//         if (finalScore[key] === result) {
//             resultWord = key
//         }
//     }
//     return resultWord
// }
// highestScoringWord(string, object)



// Task 85 (object)
// var object = {
//     'Darth Vader': "father",
//     'Leia': "sister",
//     'Han': "brother in law",
//     'R2D2': "droid",
// }
// var string = "Leia"
// function relationToLuke(string) {
//     console.log(object.Leia)
//     if (object.Leia === string) {
//         // for (key in object){
//         //  if(key === string){
//         return `Luke, I am your ${object.Leia}.`
//     }
//     //}
// }
// relationToLuke(string)



// Task 85 (object, array)
// Write the function detectNetwork. It should accept a string or a number for its cardNumber argument and,
// based on the provided cardData, return the appropriate network string (or undefined if there's no match).
// var cardData = [
//     {
//         'issuer/network': 'Visa',  // card issuer (network)
//         prefixes: ['4'],	       // beginning digits
//         lengths: [13, 16, 19]      // lengths of card numbers
//     },
//     {
//         'issuer/network': 'Mastercard',
//         prefixes: ['51', '52', '53', '54', '55'],
//         lengths: [16]
//     },
//     {
//         'issuer/network': 'American Express',
//         prefixes: ['34', '37'],
//         lengths: [15]
//     },
//     {
//         'issuer/network': 'Diner\'s Club',
//         prefixes: ['38', '39'],
//         lengths: [14]
//     }
// ];
// function detectNetwork(cardNumber, cardData) {
//   const cardPre = cardNumber.slice(0,2)
//   const cardLength = cardNumber.length
//  let result = undefined
//   for(let i =0; i< cardData.length; i++){
//    if((cardData[i].prefixes).includes(cardPre)&&(cardData[i]['lengths']).includes(+cardLength)){
//      result = cardData[i]['issuer/network']
//    }

//   }
//   return result
//another solution
// function detectNetwork(cardNumber, cardData) {
//     const cardPre = cardNumber.slice(0, 2)
//     const cardLength = cardNumber.length
//     let result = undefined
//     cardData.forEach(item => {
//         if ((item.prefixes).includes(cardPre) && (item['lengths']).includes(+cardLength)) {
//             result = item['issuer/network']
//         }
//     })
//     return result
// }
// example
// var network = detectNetwork('343456789012345', cardData);
// var network1 = detectNetwork('5113245678563427', cardData);
// console.log(network1); // --> 'American Express'



// Task 86 ( array)
// findLargestNums([[4, 2, 7, 1], [20, 70, 40, 90], [1, 2, 0]]) ➞ [7, 90, 2]
// findLargestNums([[-34, -54, -74], [-32, -2, -65], [-54, 7, -43]]) ➞ [-34, -2, 7]
//  findLargestNums([[0.4321, 0.7634, 0.652], [1.324, 9.32, 2.5423, 6.4314], [9, 3, 6, 3]]) ➞ [0.7634, 9.32, 9]
// function findLargestNums(array){
//     var result =[]
//     for(i=0; i<array.length; i++){
//       var sorted = array[i].sort((a,b)=>b-a)
//       result.push(sorted[0])
//       console.log(sorted)
//     }
//     return result
//     }
//     //another solution
//     function findLargestNums(array){
//     return array.map(item=>{
//      return item.sort((a,b)=>b-a)[0]
//     })
//     }





// Task 87 (object, array)
// // Создать функцию countOf();
//         // аргументы:
//         // - str (String | Строка в которой будет происходить поиск)
//         // - target (String | Подстрока по которой будет производится поиск)
//         // Функция должна принимать два аргумента и возвращать количество вхождений target
//         // внутри str.
//         // Поиск должен быть не чуствителен к регистру

//         // Примеры
//         // countOf('abab abcdab', 'ab') // 4
//         // countOf('AbaB ABcdab', 'ab') // 4
//         // countOf('AbaB ABcdab', 'abc') // 1
//         // countOf('AbaB ABcdab', 'zzzz') // 0

//         // console.log(countOf('abab abcdab', 'ab')) // 4
//         // console.log(countOf('AbaB ABcdab', 'ab')) // 4
//         // console.log(countOf('AbaB ABcdab', 'abc')) // 1
//         // console.log(countOf('AbaB ABcdab', 'zzzz')) // 0
//         // console.log(countOf('zzzzzz', 'zz')) // 3
//         const string = 'AbaB ABcdab'
//         const target ='ab'
//         function countOf(string, target){
//           let newString = string.toLowerCase()
//           let newTarget = target.toLowerCase()
//           let lastPosition = 0
//           let result = 0
//           while(lastPosition >-1){
//             console.log(lastPosition)
//             lastPosition = newString.indexOf(newTarget, lastPosition)
//             if(lastPosition>-1){
//               result++
//               lastPosition+=target.length
//             }
//           }
//           return result
//         }
//          countOf(string, target)




// Task 88 (object, array)
// Создать функцию fooBar()
// аргументы
// - min (Number | старт)
// - max (Number | конец)
// При вызове возвращает массив длинной равной промежутку между min и max включительно
// на каждый елемент действует следующая логика:
// Если число делится на 3 элементом должна быть строка 'foo'
// Если число делится на 5 элементом должна быть строка 'bar'
// Если число делится на 3 и 5 элементом должна быть строка 'foobar'
// Если не делится ни на 3, ни 5, то элемент равен пустой строке
// ['', '', 'foo', '', 'bar', 'foo', '','','foo','bar'...'foobar'];
// const min =1
// const max = 30
// function fooBar(min, max){
// const array = []
// for (let i =min; i<=max;i++){
// if(i%3===0){
//   array.push("foo")
// }else if(i%5===0){
//   array.push("bar")
// }else if(i%3===0 && i%5===0){
//   array.push("foobar")
// }else{
//   array.push("")
// }
// let string = ""
// if(i%3===0)string+="foo"
// if(i%5===0)string+="bar"
// if(i%3===0 && i%5===0)string+="foobar"
// array.push("")
// }
// return array
// }
// fooBar(min, max)



// Task 89 (string, number)
// 1. Write a JavaScript function that reverse a number. Go to the editor
// const x = 32243;
// Expected Output : 34223
// function reverseNumber(num){
//   var string = String(num)
//   var splitted = +(string.split("").reverse().join(""))
//  return splitted
// }
// reverseNumber(x)



// Task 90 (string, palindrom)
// 2. Write a JavaScript function that checks whether a passed string is palindrome or not? Go to the editor
// A palindrome is word, phrase, or sequence that reads the same backward as forward, e.g., madam or nurses run.
// Click me to see the solution
// var str="1234543212"
// var expected = "123454321"
// function isPalindrom(string){
//   var result = string.split('').reverse().join("")
//   console.log(result)
//   if(string===result){
//     return true
//   }
//   return false
// }
// isPalindrom(str)



// Task 91 (string, sort)
// 4. Write a JavaScript function that returns a passed string with letters in alphabetical order. Go to the editor
// Example string : 'webmaster'
// Expected Output : 'abeemrstw'
// Assume punctuation and numbers symbols are not included in the passed string.
// Click me to see the solution
// var str =  'webmaster'
// function sortLetters(string){
//   return string.split("").sort().join("")
// }
// sortLetters(str)



// Task 91 (string)
// 5. Write a JavaScript function that accepts a string as a parameter and
// converts the first letter of each word of the string in upper case. Go to the editor
// Example string : 'the quick brown fox'
// Expected Output : 'The Quick Brown Fox '
// Click me to see the solution
// var  string = 'the quick Brown fox'
// function convertsLetters(string){
// var lower = string.toLowerCase()
// var result = ""
// var array = lower.split(' ')
// console.log(array)
// for(i=0; i<array.length; i++){
//   result+=array[i][0].toUpperCase()+array[i].slice(1)+" "
// }
// console.log(result)
// }
// convertsLetters(string)


// Task 92 (string)
// 6. Write a JavaScript function that accepts a string as
// a parameter and find the longest word within the string. Go to the editor
// Expected Output : 'Development'
// Click me to see the solution
// var string = 'Web Development Tutorial'
// function findlongest(str){
// var splitted = str.split(' ')
// var longest = splitted[0]
// for(var i=1; i<splitted.length; i++){
//   if(splitted[i].length>longest.length){
//     longest = splitted[i]
//   }
// }
// return longest
// }
// findlongest(string)


// Task 93 (string)
// 7. Write a JavaScript function that accepts a string as a parameter and counts the number 
//of vowels within the string. Go to the editor
// Note : As the letter 'y' can be regarded as both a vowel and a consonant,
// we do not count 'y' as vowel here.
// Example string : 'The quick brown fox'
// Expected Output : 5
// Click me to see the solution
// var string = 'The quick brown fox'
// var vowels = ['a', 'e', 'u', 'i', 'o']
// function countVowels(str, arr){
// var result = 0;
// for(i=0; i<str.length; i++){
//   for(j=0; j<arr.length; j++){
// if(str[i]===arr[j]){
//     result+=1
//   }
//   }
// }
// return result
// }
// countVowels(string, vowels)


// Task 94 (numbers)
// . Write a JavaScript function that accepts a number as a parameter and check the number is prime or not.
// Go to the editor
// Note : A prime number (or a prime) is a natural number greater
// than 1 that has no positive divisors other than 1 and itself.
// Click me to see the solution
// var number = 12
// function checkNumber(num){
//   if(num===1){
//     return 'prime'
//   }
// for (i=2; i<num; i++){
//   if(num%i===0){
//     return "not prime"
//   }
//   return "prime"
// }
// }
// checkNumber(number)




// Task 94 (typeof)
// 9. Write a JavaScript function which accepts an argument and returns the type. Go to the editor
// // Note : There are six possible values that typeof returns: object, boolean, function, number, string, and undefined.
// // Click me to see the solution
// // boolean, object, function
// var argument= []
// function getType(arg){
//   if(arg===null){
//     return 'null'
//   }
//   if(Array.isArray(arg)){
//     return 'array'
//   }
// return typeof(arg)
// }
// getType(argument)



// Task 95 (array, object)
// var temperatures=[{city:"New York", temperature:4}, {city:"Vladivostok", temperature:12}, {city:"Tokyo", temperature:20}, {city:"Paris", temperature:15}, {city:"London", temperature:7}]
// function getTemperature(temperatures){
// var coldest = 0
// var hotest = 0
// var cityColdest = ""
// var cityHotest = ""
// for (var i=0; i<temperatures.length; i++){
//   var city = temperatures[i]
//   if(city['temperature']<coldest)
// }
// another solution
// function getTemperature(temperatures){
//    var coldest = 0
//   var hotest = 0
//   var sorted = temperatures.sort((a,b)=>{
//     return a['temperature']- b['temperature']
//   })
//   coldest = sorted[0].city
//   hotest = sorted[sorted.length-1].city
//   console.log(sorted[0].city)
//   var result = {coldest, hotest} 
// }
// getTemperature(temperatures)




// Task 96 (array)
// var array = [2, 3, 4, 5, 6]
// var item =5
// function indexOfArray(array, item) {
//   // your code here
//   // console.log(array.indexOf(item))
//   if(array.indexOf(item)=== undefined){
//       return -1
//   }else{
//   return array.indexOf(item)
//   }
// }
// indexOfArray(array, item)


// Task 97 (object)
// function countAllCharacters(str) {
//   var obj={}
//   for(i=0; i<str.length; i++){
//     if(str[i] in obj){
//     obj[str[i]]++
//     }else{
//     obj[str[i]]=1
//     }
//   }
//   return obj
// }
// var output = countAllCharacters('banana');
// console.log(output); // --> {b: 1, a: 3, n: 2}


// Task 98(array, object)
//  Создать функцию findMostPopular()
//  Функция должна принимать массив в виде аргумента и возвращать самый
//  часто повторяющийся в нем элемент.
//  Условие гарантирует что такой элемент будет один.
//  Примеры:
// console.log(findMostPopular([1, 2, 3, 2, 1, 3, 3, 4])) // 3
// console.log(findMostPopular(['z', 12, 'z', 'dd', 13, 10])) // 'z'
// function findMostPopular(array) {
//     var obj = {}
//     var highest = {
//         key: "",
//         count: 0
//     }
//     for (var i = 0; i < array.length; i++) {
//         if (array[i] in obj) {
//             obj[array[i]]++
//         } else {
//             obj[array[i]] = 1
//         }
//     }
//     for (var key in obj) {
//         if (highest['count'] < obj[key]) {
//             highest['count'] = obj[key]
//             highest['key'] = key
//         }
//     }
//     return highest["key"]
// }




// Task 99(array, object)
// function mapping(letters) {
// var object = {}
// console.log(object)
// for (var i = 0; i< letters.length; i++){
//   var key = letters[i]
//   var value =letters[i].toUpperCase()
//  object[key]=value
//   }
//   console.log(object)
// return object
// }
// mapping(["p", "s"]) // { "p": "P", "s": "S" }
// mapping(["a", "b", "c"]) // { "a": "A", "b": "B", "c": "C" }
// mapping(["a", "v", "y", "z"]) // { "a": "A", "v": "V", "y": "Y", "z": "Z" }


// Task 100(array, object)
// function afterNYears(names, n) {
//   for (var key in names){
//     names[key]= names[key]+Math.abs(n)
//   }
//   return names
// }
// afterNYears({
//   "Joel" : 32,
//   "Fred" : 44,
//   "Reginald" : 65,
//   "Susan" : 33,
//   "Julian" : 13
// }, 1)
//{
//   "Joel" : 33,
//   "Fred" : 45,
//   "Reginald" : 66,
//   "Susan" : 34,
//   "Julian" : 14
// }
// afterNYears({
//   "Baby" : 2,
//   "Child" : 8,
//   "Teenager" : 15,
//   "Adult" : 25,
//   "Elderly" : 71
// }, 19) //{
//   "Baby" : 21,
//   "Child" : 27,
//   "Teenager" : 34,
//   "Adult" : 44,
//   "Elderly" : 90
//}
// afterNYears({
//   "Genie" : 1000,
//   "Joe" : 40
// }, 5)
// {
//   "Genie" : 1005,
//   "Joe" : 45
// }



// Task 101(array, object)
// const scores = {"A": 100, "B": 14, "C": 9, "D": 28, "E": 145, "F": 12, "G": 3,
// "H": 10, "I": 200, "J": 100, "K": 114, "L": 100, "M": 25,
// "N": 450, "O": 80, "P": 2, "Q": 12, "R": 400, "S": 113,
// // "T": 405, "U": 11, "V": 10, "W": 10, "X": 3, "Y": 210, "Z": 23}
// const scores = {"A": 100, "B": 14, "C": 9, "D": 28, "E": 145, "F": 12, "G": 3,
// "H": 10, "I": 200, "J": 100, "K": 114, "L": 100, "M": 25,
// "N": 450, "O": 80, "P": 2, "Q": 12, "R": 400, "S": 113, "T": 405,
// "U": 11, "V": 10, "W": 10, "X": 3, "Y": 210, "Z": 23};
// function nameScore(name) {
//   var total =0
//   for (var i=0; i<name.length; i++){

// 	for(var key in scores){
//     if(key === name[i]){
//       total+=scores[key]

//     }
//   }
//   }
//   console.log(total)
//   if(total<=60 ){
//     return  "NOT TOO GOOD"
//   }else if(total >60 && total <=300){
//     return "PRETTY GOOD"
//   } else if (total >300 && total<600){
//     return "VERY GOOD"
// }else {
//   return "THE BEST"
// }
// }
// nameScore("MUBASHIR") // "THE BEST"
// nameScore("YOU") // "VERY GOOD"
// nameScore("MATT") // "THE BEST"
//nameScore("PUBG") //"NOT TOO GOOD"



// Task 102(array, object)
// const obj = {
//   tv: 30,
//   timmy: 20,
//   stereo: 50,
// } // "Timmy is gone..."
// Timmy is in the object.
// const obj1 = {
//   tv: 30,
//   stereo: 50,
// } 
// "Timmy is here!"
// Timmy is not in the stolen list object.
// const obj2 = { } // "Timmy is here!"
// Timmy is not in the object.
// function findIt(obj, name) {
//   var arr = Object.keys(obj)
//   console.log(arr)
//   if (arr.includes(name)){
//     return `${name} is gone`
//   } 
// return `${name} is here `
// for(var key in obj){
//  if(key === name){
//    return `${name} is gone `
//  }else {
//    return `${name} is here`
//  }
// }
//   var keyObject = obj.hasOwnProperty(name)
//   console.log(keyObject)
//  }
// findIt(obj1, "timmy")



// Task 103(, object)!!!!!!
// class Name {
// }
// a1 = new Name("john", "SMITH")
// a1.fname // "John"
// a1.lname // "Smith"
// a1.fullname //"John Smith"
// a1.initials // "J.S"
// var alternativeInput = {
//   a : 'a',
//   number : 11,
//   hungry : true,
//   grammyWins : 1
// };



// Task 104(array, object)
// function getAllKeys(obj) {
//   // your code here
//   var result= []
//   for(var key in obj){
//     result.push(key)
//   }
//   return result
// }
// var actual = getAllKeys(alternativeInput)
// var expected = [ 'a', 'number', 'hungry', ]
// var testName = "should returns keys"
// function assertEqual(actual, expected, testName){
//   actual = JSON.stringify(actual)
//   expected = JSON.stringify(expected)
//   if(actual===expected){
//     console.log(`passed`)
//   }else{
//     console.log(`FAILED '${testName}' Expected ${expected}, but got ${actual}`)
//   }
// }
// assertEqual(actual, expected, testName)
// var input = [['make', 'Ford'], ['model', 'Mustang'], ['year', 1964]];

// function transformArrayToObject(array) {
//   // your code here
//   var result={}
//   for (var i = 0; i<array.length;i++){
//     var arr = array[i]
//     result[arr[0]]=arr[1]
//   }
// return result
// }
// transformArrayToObject(input)
//  var actual = transformArrayToObject(input)
//  var expected = {
//   make : 'Ford',
//   model : 'Mustang',
//   year : 1964,
//   color: "red"
// }
// var testName = "should return object"
//  function assertEqual(actual, expected, testName){
//    var actual =JSON.stringify(actual)
//    var expected = JSON.stringify(expected)
//    if (actual ===expected ){
//      console.log(`passed`)
//    }else {
//      console.log(`FAILED '${testName}' Expected ${expected}, but got ${actual}`)
//    }
//  }
//  assertEqual(actual, expected, testName)
//  var expected= {
//   make : 'Ford',
//   model : 'Mustang',
//   year : 1964
// }
// var testName = " should return object"
// function assertEqual(actual, expected, testName){
//   actual = JSON.stringify(actual)
//   expected = JSON.stringify(expected)
//   if(actual===expected){
//     console.log(`passed`)
//   }else{
//     console.log(`FAILED '${testName}' Expected ${expected}, but got ${actual}`)
//   }
// }
// assertEqual(actual, expected, testName)




// Task 105(array, object)
// var input = {
//   name : 'Krysten',
//   age : 33,
//   hasPets : false,
// };
// function listAllValues(obj) {
//   // your code here
//   var result =[]
//   for(var key in obj){

//     result.push(obj[key])
//   }
//   console.log(result)
//   return result
//   //return Object.values(obj)
// }
// var actual = listAllValues(input)
// var expected = ['Krysten', 33, false]
// var testName = "should return arr"
// function assertEqual(actual, expected, testName){
//  actual = JSON.stringify(actual)
//  expected = JSON.stringify(expected)
//   if(actual ===expected){
//     console.log(`passed`)
//   }else{
//     console.log(`FAILED '${testName}'' Expected ${expected}, but got ${actual}`)
//   }
// }
// assertEqual(actual, expected, testName)




// Task 106(array, object)
// var input = [
//     [
//         ['firstName', 'Joe'], ['lastName', 'Blow'], ['age', 42], ['role', 'clerk']
//     ],
//     [
//         ['firstName', 'Mary'], ['lastName', 'Jenkins'], ['age', 36], ['role', 'manager']
//     ]
// ];
// function transformEmployeeData(employeeData) {
//   let object ={}
//   let result = []
//   for(let i=0; i<employeeData.length; i++ ){
//     var person= employeeData[i]
//     //console.log(employeeData[i])
//    for (let j =0; j<person.length;j++){
//      object[person[j][0]]=person[j][1]
//    }
// result.push(object)
//   }
//   return result
// }
// transformEmployeeData(input)



// Task 107(array, object)
// var input = {
//   name: 'Holly',
//   age: 35,
//   role: 'producer'
// };
// function convertObjectToArray(obj) {
//   var array = []
//   for (var key in obj){
//     array.push([key,obj[key]])
//   }
//   return array
// }
// var expected =[['name', 'Holly'], ['age', 35], ['role', 'producer']]
// convertObjectToArray(input)



// Task 108(array, object)
// function findShortestElement(arr) {
//    var shortest = ""
//   if (arr.length ===0){
//     return shortest
//   }else{
//     shortest = arr[0]
//   for(var i = 1; i<arr.length;i++){
//     if(arr[i].length<shortest.length){
//       shortest = arr[i]
//     }
//     return shortest
//   }
//   }
// }
// var output = findShortestElement(["a", "b"]);



// Task 109(array, object)
// function filterOddLengthWords(words) {
//   var result = [];
//   for (var i=0; i<words.length; i++){
//     if(words[i].length%2===1){
//       result.push(words[i])
//     }
//   }
//  return result
// }
// var output = filterOddLengthWords(['there', 'it', 'is', 'now']);
// console.log(output); // --> ['there', "now']



// Task 110(array, object)
// function getLargestElement(arr) {
//   // your code here
//   if(arr.length===0){
//     return 0
//   }
//   var largest = arr[0]
//   for (var i=1; i<arr.length; i++){
//     if(arr[i]>largest){
//       largest=arr[i]
//     }
//   }
//   return largest
// }
// getLargestElement([5, 2, 8, 3]);



// Task 111(arrayt)
// function computeSumOfAllElements(arr) {
//   return arr.reduce((acc,curent)=>{
//     return acc+curent
//   }, 0)
//   return acc
// }
// computeSumOfAllElements([1, 2, 3])



// Task 112(Math)
// function calculateBillTotal(preTaxAndTipAmount) {
//   var result = 0
//   var tax = preTaxAndTipAmount*0.095
//   var tip = preTaxAndTipAmount*0.15
//   return result = preTaxAndTipAmount +tax +tip
// }
// calculateBillTotal(20);



// Task 113(string)
// function getStringLength(string) {
//   var result = 0
//  for(var i=0; i<string.length; i++){
//    result+=1
//  }
//  return result
// }
// another solution
// function getStringLength(string) {
// var result = string.slice()
// return result
// }
// var output = getStringLength('hello');




// Task 113(array, )
// var input = [
//     [
//         ['firstName', 'Joe'], ['lastName', 'Blow'], ['age', 42], ['role', 'clerk']
//     ],
//     [
//         ['firstName', 'Mary'], ['lastName', 'Jenkins'], ['age', 36], ['role', 'manager']
//     ]
// ];
// function transformEmployeeData(employeeData) {
//   var result = []
//   for (var i= 0; i< employeeData.length; i++){
//     var person = employeeData[i]
// var object ={}
// for (var j =0; j<person.length; j++){
//     var key = person[j][0]
//     var value = person[j][1]
//     object[key]=value
//       }
//       result.push(object)
//     }
//    return result
//   }
// transformEmployeeData(input)



// Task 113(array, )
// function joinArrayOfArrays(arr) {
//   var result = []
//   for (var i =0; i <arr.length; i++){
//      result=result.concat(arr[i])
//   }
//   return result
// }
// var output = joinArrayOfArrays([[1, 4], [true, false], ['x', 'y']]);
// console.log(output); // --> [1, 4, true, false, 'x', 'y']


// Task 114(array, object )
// var obj = {
//   key: [1, 2, 3, 4]
// };
// var output = getProductOfAllElementsAtProperty(obj, 'key');
// console.log(output); // --> 24
// function getProductOfAllElementsAtProperty(obj, key) {
//   If the array is empty, it should return 0.
// If the property at the given key is not an array, it should return 0.
// If there is no property at the given key, it should return 0.
//     var arr = obj['key']
//     if (arr.length === 0) {
//         console.log(arr.length)
//         return 0
//     }
//     if (!Array.isArray(obj['key'])) {
//         return 0
//     }
//     var result = 1
//     for (var i = 0; i < arr.length; i++) {
//         result *= arr[i]
//     }
//     return result
// }
// getProductOfAllElementsAtProperty(obj = { key: [], })


// Task 114(object )
// function convertToNumber(obj) {
//     for (key in obj) {
//         obj[key] = +obj[key]
//     }
//     return obj
// }
// convertToNumber({ piano: "200" }) // { piano: 200 }
// convertToNumber({ piano: "200", tv: "300" }) //{ piano: 200, tv: 300 }
// convertToNumber({ piano: "200", tv: "300", stereo: "400" }) // { piano: 200, tv: 300, stereo: 400 }


// Task 115(object )
// In this challenge, you have to convert a weight weighed on a planet of the Solar System to the corresponding weight on another planet.
// To convert the weight, you have to divide it by the gravitational force of the planet on which is weighed and multiply the result (the mass) for the gravitational force of the other planet. See the table below for a list of gravitational forces:
// weight on planetA / gravitational force of planetA * gravitational force of planetB
// Given a weight weighed on planetA, return the converted value for planetB rounded to the nearest hundredth.
// var planets={
//     Mercury	:3.7,
//     Venus	:8.87,
//     Earth	:9.81,
//     Mars	:3.711,
//     Jupiter	:24.79,
//     Saturn	:10.44,
//     Uranus:	8.69,
//     Neptune:	11.15,
//     }
//     console.log(planets)
//     function spaceWeights(planetA, weight, planetB) {
//         return (weight/planets[planetA]*planets[planetB]).toFixed(2)
//     }
//     spaceWeights("Earth", 1, "Mars") // 0.38
// spaceWeights("Earth", 1, "Jupiter") // 2.53
// spaceWeights("Earth", 1, "Neptune") // 1.14



// Task 116(object )
// function mostExpensiveItem(obj) {
//     var stolenItem = ""
//     var result = 0
//     for (key in obj) {
//         if (obj[key] > result) {
//             result = obj[key]
//             stolenItem = key
//         }
//     }
//     return stolenItem
// }
// mostExpensiveItem({
//   piano: 2000,
// }) // "piano"
// mostExpensiveItem({
//   tv: 30,
//   skate: 20,
// }) // "tv"
// mostExpensiveItem({
//     tv: 30,
//     skate: 20,
//     stereo: 50,
//   }) // "stereo"



// Task 117(object )
// function countAll(str) {
// 	var result = {
//     "LETTERS":0,
//     "DIGITS":0,
//   }
//   for(i=0; i<str.length; i++){
//   if(str[i]===" "){
//     result['LETTERS']+=0
//   }else if(isNaN(str[i])){
//      result['LETTERS']++
//    }
//    else{
//      result['DIGITS']++
//    }

//   }
//   return result
// }
// countAll("Hello World") // { "LETTERS":  10, "DIGITS": 0 }
// countAll("H3ll0 Wor1d") // { "LETTERS":  7, "DIGITS": 3 }
// countAll("149990") // { "LETTERS": 0, "DIGITS": 6 }




// Task 118(object )
// function getXP(obj) {
// 	var points={
// 'Very Easy':'5XP',
// 'Easy' : '10XP',
// 'Medium':	'20XP',
// 'Hard':	'40XP',
// 'Very Hard'	:'80XP',
//   }
// var result =0
// for(key in obj){
//  if(key === 'Very Easy'){
// result+=obj[key]*5
//  }else if(key === 'Easy'){
// result+=obj[key]*10
// }else if(key === 'Medium'){
// result+=obj[key]*20
// }else if(key === 'Hard'){
// result+=obj[key]*40
// }else if(key === 'Very Hard'){
// result+=obj[key]*80
// }
// }
// return `${result}XP`
// }
// another solution
// const getXP = obj => Object.values(obj)
//  .reduce((t, c, i) => t + [5, 10, 20, 40, 80][i] * c, 0) + `XP`;
// getXP({
//   "Very Easy" : 89,
//   "Easy" : 77,
//   "Medium" : 30,
//   "Hard" : 4,
//   "Very Hard" : 1
// }) // "2055XP"
// getXP({
//   "Very Easy" : 254,
//   "Easy" : 32,
//   "Medium" : 65,
//   "Hard" : 51,
//   "Very Hard" : 34
// }) // "7650XP"
// getXP({
//   "Very Easy" : 11,
//   "Easy" : 0,
//   "Medium" : 2,
//   "Hard" : 0,
//   "Very Hard" : 27
// }) // "2255XP"




// Task 119(object )
// function expensiveOrders(orders, cost) {
// 	var result ={}
// for(key in orders){
//   if(orders[key]>cost){
//     result[key]=orders[key]
//   }
// }
//   return result
// }
// expensiveOrders({ "a": 3000, "b": 200, "c": 1050 }, 1000)
// { "a": 3000, "c": 1050 }
// expensiveOrders({ "Gucci Fur": 24600, "Teak Dining Table": 3200, "Louis Vutton Bag": 5550, "Dolce Gabana Heels": 4000 }, 20000)
// // { "Gucci Fur": 24600 }
// expensiveOrders({ "Deluxe Burger": 35, "Icecream Shake": 4, "Fries": 5 }, 40) // {}




// Task 120(object)
// const GUEST_LIST = {
// 	Randy: "Germany",
// 	Karla: "France",
// 	Wendy: "Japan",
// 	Norman: "England",
// 	Sam: "Argentina"
// }
// function greeting(name) {
// if(GUEST_LIST[name]===undefined){
//   return "Hi! I'm a guest."
// }
//  return `Hi! I'm ${name}, and I'm from ${GUEST_LIST[name]}.`
// }
// greeting("Randy") // "Hi! I'm Randy, and I'm from Germany."
// greeting("Sam") // "Hi! I'm Sam, and I'm from Argentina."
// greeting("Monti") // "Hi! I'm a guest."